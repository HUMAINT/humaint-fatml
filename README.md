# HUMAINT-FATML framework

###Features

- Train machine learning models with tabular data;
- Easy plug-and-play experiment configuration through ini files;
- Evaluate group fairness of ML models;
- Use ML interpretability (LIME) to interpret the results;

### Instalation
We use conda with the packages listed in requirements.txt and in requirements-pip.txt.

To create the environment from the requirements file:

`$ conda create --name fatml --file requirements.txt`

Activate the environment with:

`$ source activate fatml`

Then install the pip packages from requirements-pip.txt:

`$ pip install -r requirements-pip.txt`

The pytorch from conda official package has some bugs so we install the official pytorch:

`$ conda update pytorch -c soumith`

Run a quick test:

`$ python main.py --kfolds 10 --interpretability 0 --fold 0 --experiment "exp1_savry" --approach "logit" --seed 0`


### Experiments


##### Reproduce an experiment

We have reproduced the following experiments/datasets with the corresponding ini files in the experiments folder:

+ COMPAS in compas.ini
+ SAVRY
    + exp1_savry.ini
    + exp1_demog.ini
    + exp1_demogsavry.ini
+ GERMAN in german.ini
+ CREDIT in credit.ini


##### Create an experiment

An ini file has the following structure:
```ini
[dataset]
filename=
filename_test=
url=
[experiment]
label_column=
protected_features=
drop_columns =
drop_train=
group_column=
```
The field `filename` must contain the name of the tabular file where the data is stored. If another file is used for test, the name of this file must be written under `filename_test`. If the file is not stored locally in the dat folder, it should be stored at the `url`.

The field `filename` must contain the label column in the tabular data which should be binary. For the fairness analysis we need a set of `protected_features` which should be given as a list. You can drop columns which won't be used during the analysis or solely during training.

### Bibliography

##### ICAIL 2019: Fairness in youth recidivism prediction using machine learning

>Tolan S, Miron M, Gomez E, Castillo C, Why Machine Learning May Lead to Unfairness: Evidence from Risk Assessment for Juvenile Justice in Catalonia

To train the models run:

`$ bash run_icail.sh`

To generate the plots and the results in the paper run:

`$ python results_icail.py --allexp 1 --experiment 'exp1_demog' --allseed 1 --nseeds 40`

Run the script without the flags `--allseed 1 --nseeds 40` to reproduce the results for a single seed.

##### ECML 2019

To train the models run:

`$ bash run_ecml.sh`

To generate the plots and the results in the paper run:

`$ python results_ecml.py --allexp 1 --experiment 'compas' --allseed 1 --nseeds 10`

Run the script without the flags `--allseed 1 --nseeds 10` to reproduce the results for a single seed.

### Copyright
Copyright [2019] [European Commission]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
