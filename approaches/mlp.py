import sys,time,os
import numpy as np
import torch

import utils

class Appr(object):

    def __init__(self,model,outfile,score_func,nepochs=100,sbatch=128,lr=0.05,lr_min=1e-5,lr_factor=3,lr_patience=20,clipgrad=10000,args=None,weights=None):
        self.model=model

        self.nepochs=nepochs
        self.sbatch=sbatch
        self.lr=lr
        self.lr_min=lr_min
        self.lr_factor=lr_factor
        self.lr_patience=lr_patience
        self.clipgrad=clipgrad
        self.outfile=outfile
        self.score_func=score_func

        if weights is not None:
            self.criterion = utils.WeightedBCELoss(
                pos_weight=torch.from_numpy(np.ones(2)).type(torch.FloatTensor).cuda(), weight=weights)
        else:
            self.criterion = torch.nn.BCELoss()
        self.optimizer=self._get_optimizer()

        if args.load:
            if os.path.isfile(args.outfile):
                print("loading checkpoint '{}'".format(args.outfile))
                checkpoint = torch.load(args.outfile)
                self.start_epoch = checkpoint['epoch']
                train_loss = checkpoint['loss']
                self.model.load_state_dict(checkpoint['state_dict'])
                self.optimizer.load_state_dict(checkpoint['optimizer'])
                print("loaded checkpoint '{}' (epoch {})"
                      .format(args.outfile, checkpoint['epoch']))
            else:
                print("no checkpoint found at '{}'".format(args.outfile))

        return

    def _get_optimizer(self,lr=None):
        if lr is None: lr=self.lr
        return torch.optim.Adam(self.model.parameters(),lr=lr,weight_decay=0.01, amsgrad=False)#

    def train(self,xtrain,ytrain,xvalid,yvalid):
        best_loss=np.inf
        best_model=utils.get_model(self.model)
        lr=self.lr
        patience=self.lr_patience
        self.optimizer=self._get_optimizer(lr)

        # Loop epochs
        for e in range(self.nepochs):
            # Train
            clock0=time.time()
            self.train_epoch(xtrain,ytrain)
            clock1=time.time()
            train_loss,acc=self.eval(xtrain,ytrain)
            clock2=time.time()

            print('| Epoch {:3d}, time={:5.1f}ms/{:5.1f}ms | Train: loss={:.3f},auc={:.3f} |'.format(e+1,1000*self.sbatch*(clock1-clock0)/xtrain.size(0),1000*self.sbatch*(clock2-clock1)/xtrain.size(0),train_loss,acc),end='')
            # Valid
            valid_loss,valid_acc=self.eval(xvalid,yvalid)
            print(' Valid: loss={:.3f},auc={:.3f} |'.format(valid_loss,valid_acc),end='')
            ##Adapt lr
            if valid_loss<best_loss:
                best_loss=valid_loss
                best_model=utils.get_model(self.model)
                # patience=self.lr_patience
                # print(' *',end='')
                torch.save({'epoch': e + 1, 'state_dict': self.model.state_dict(), 'loss': train_loss,
                            'optimizer': self.optimizer.state_dict()}, self.outfile)
            else:
                patience-=1
                if patience<=0:
                    lr/=self.lr_factor
                    print(' lr={:.1e}'.format(lr),end='')
                    if lr<self.lr_min:
                        print()
                        break
                    patience=self.lr_patience
                    self.optimizer=self._get_optimizer(lr)
            # torch.save({'epoch': e + 1, 'state_dict': self.model.state_dict(), 'loss': train_loss,
            #            'optimizer': self.optimizer.state_dict()}, self.outfile)
            print()

        ##Restore best
        utils.set_model_(self.model,best_model)

        return

    def train_epoch(self,x,y):
        self.model.train()

        with torch.enable_grad():
            r=np.arange(x.size(0))
            np.random.shuffle(r)
            if torch.cuda.is_available():
                r=torch.LongTensor(r).cuda()
            else:
                r=torch.LongTensor(r)

            # Loop batches
            for i in range(0,len(r),self.sbatch):
                if i+self.sbatch<=len(r): b=r[i:i+self.sbatch]
                else: b=r[i:]

                points=torch.autograd.Variable(x[b])
                targets=torch.autograd.Variable(y[b])

                # Forward
                outputs=self.model.forward(points)
                #import pdb;pdb.set_trace()
                loss=self.criterion(outputs,targets)

                # for i, j in zip(targets, outputs):
                #     print(i.data[0], j.data[0])
                # import pdb;
                # pdb.set_trace()

                # Backward
                self.optimizer.zero_grad()
                loss.backward()
                torch.nn.utils.clip_grad_norm_(self.model.parameters(),self.clipgrad)
                self.optimizer.step()

        return

    def eval(self, x, y):
        with torch.no_grad():
            total_loss = 0
            total_acc = 0
            total_num = 0
            self.model.eval()

            r = np.arange(x.size(0))
            if torch.cuda.is_available():
                r = torch.LongTensor(r).cuda()
            else:
                r=torch.LongTensor(r)
            sbatch = np.minimum(len(r),self.sbatch)

            pred = []
            # Loop batches
            for i in range(0, len(r), sbatch):
                if i + sbatch <= len(r):
                    b = r[i:i + sbatch]
                else:
                    b = r[i:]
                points = torch.autograd.Variable(x[b])
                targets = torch.autograd.Variable(y[b])

                # Forward
                output = self.model.forward(points)
                loss = self.criterion(output, targets)
                pred.append(output.data.cpu().numpy())
                # _, pred = output.max(1)
                # import pdb;
                # pdb.set_trace()
                # hits = (pred == targets.type(torch.LongTensor).cuda()).float()

                total_loss += float(loss.data.cpu().numpy() * len(b))
                # total_acc += float(hits.sum().data.cpu().numpy())
                total_num += len(b)
            if len(pred)>1:
                temp = np.stack(pred[:-1], axis=0)
                temp = np.append(temp, pred[-1])
            else:
                temp = pred[-1]
            pred = None

            return total_loss / total_num, self.score_func(y.data.cpu().numpy(),temp) #self.score_func(temp > 0.5, y.data.cpu().numpy())

    def predict(self,x):
        with torch.no_grad():
            self.model.eval()

            r=np.arange(x.size(0))
            if torch.cuda.is_available():
                r = torch.LongTensor(r).cuda()
            else:
                r=torch.LongTensor(r)
            sbatch = np.minimum(len(r), self.sbatch)

            pred=[]
            # Loop batches
            for i in range(0,len(r),sbatch):
                if i+sbatch<=len(r): b=r[i:i+sbatch]
                else: b=r[i:]
                points=torch.autograd.Variable(x[b])

                # Forward
                output=self.model.forward(points)
                pred.append(output.data.cpu().numpy())

            if len(pred)>1:
                temp = np.stack(pred[:-1], axis=0)
                temp = np.append(temp, pred[-1])
            else:
                temp = pred[-1]
            pred=None

            return temp

    def predict_proba(self, x):
        with torch.no_grad():
            self.model.eval()

            r = np.arange(x.size(0))
            if torch.cuda.is_available():
                r = torch.LongTensor(r).cuda()
            else:
                r=torch.LongTensor(r)
            sbatch = np.minimum(len(r), self.sbatch)

            pred = []
            # Loop batches
            for i in range(0, len(r), sbatch):
                if i + sbatch <= len(r):
                    b = r[i:i + sbatch]
                else:
                    b = r[i:]
                points = torch.autograd.Variable(x[b])

                # Forward
                output = self.model.forward(points)
                pred.append(output.data.cpu().numpy())

            if len(pred) > 1:
                temp = np.stack(pred[:-1], axis=0)
                temp = np.append(temp, pred[-1])
            else:
                temp = pred[-1]
            new_temp = np.vstack([1 - temp,temp]).T
            # import pdb;
            # pdb.set_trace()
            return new_temp
