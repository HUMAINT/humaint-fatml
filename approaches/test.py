import os
import time

import numpy as np
import torch

from old import pseudoInverse
from old.pseudoInverse import pseudoInverse


class Appr(object):

    def __init__(self,model,outfile,nepochs=100,sbatch=32,lr=0.05,lr_min=1e-4,lr_factor=3,lr_patience=5,clipgrad=10000,score_func=None,args=None,weights=None):
        self.model=[model for i in range(nepochs)]

        self.nepochs=nepochs
        self.sbatch=sbatch
        self.lr=lr
        self.lr_min=lr_min
        self.lr_factor=lr_factor
        self.lr_patience=lr_patience
        self.clipgrad=clipgrad
        self.outfile=outfile
        self.weights=weights
        self.score_func = score_func

        self.criterion=torch.nn.BCELoss()
        self.optimizer=[self._get_optimizer(i) for i in range(nepochs)]

        if args.load:
            if os.path.isfile(args.outfile):
                print("loading checkpoint '{}'".format(args.outfile))
                checkpoint = torch.load(args.outfile)
                self.start_epoch = checkpoint['epoch']
                train_loss = checkpoint['loss']
                for i in range(self.nepochs):
                    self.model[i].load_state_dict(checkpoint['state_dict'])
                print("loaded checkpoint '{}' (epoch {})"
                      .format(args.outfile, checkpoint['epoch']))
            else:
                print("no checkpoint found at '{}'".format(args.outfile))

        return

    def _get_optimizer(self,idx,lr=None):
        if lr is None: lr=self.lr
        #return torch.optim.Adam(self.model.parameters(),lr=lr)
        return pseudoInverse(params=self.model[idx].parameters(), C=0.001, L=0)

    def train(self,xtrain,ytrain,xvalid,yvalid):
        best_loss=np.inf
        #best_model=utils.get_model(self.model)
        lr=self.lr
        patience=self.lr_patience
        self.optimizer=[self._get_optimizer(i,lr) for i in range(self.nepochs)]

        # Loop epochs
        for e in range(1):
            # Train
            clock0=time.time()
            self.train_epoch(xtrain,ytrain,e)
            clock1=time.time()
            train_loss,acc=self.eval(xtrain,ytrain,e)
            clock2=time.time()

            print('| Epoch {:3d}, time={:5.1f}ms/{:5.1f}ms | Train: loss={:.3f},acc={:.3f} |'.format(e+1,1000*self.sbatch*(clock1-clock0)/xtrain.size(0),1000*self.sbatch*(clock2-clock1)/xtrain.size(0),train_loss,acc),end='')
            # Valid
            valid_loss,valid_acc=self.eval(xvalid,yvalid, e)
            print(' Valid: loss={:.3f},acc={:.3f} |'.format(valid_loss,valid_acc),end='')
            # Adapt lr
            # if valid_loss<best_loss:
            #     best_loss=valid_loss
            #     best_model=utils.get_model(self.model)
            #     patience=self.lr_patience
            #     print(' *',end='')
            #     torch.save({'epoch': e + 1, 'state_dict': self.model.state_dict(), 'loss': train_loss,
            #                 'optimizer': self.optimizer.state_dict()}, self.outfile)
            # else:
            #     patience-=1
            #     if patience<=0:
            #         lr/=self.lr_factor
            #         print(' lr={:.1e}'.format(lr),end='')
            #         if lr<self.lr_min:
            #             print()
            #             break
            #         patience=self.lr_patience
            #         self.optimizer=self._get_optimizer(lr)
            torch.save({'epoch': e + 1, 'state_dict': self.model[e].state_dict(), 'loss': train_loss,
                        }, self.outfile)
            print()

        # Restore best
        #utils.set_model_(self.model,best_model)

        return

    def train_epoch(self,x,y,idx):
        self.model[idx].train()
        r=np.arange(x.size(0))
        np.random.shuffle(r)
        r=torch.LongTensor(r).cuda()

        self.sbatch=x.size(0)

        # Loop batches
        for i in range(0,len(r),self.sbatch):
            if i+self.sbatch<=len(r): b=r[i:i+self.sbatch]
            else: b=r[i:]

            points=torch.autograd.Variable(x[b], requires_grad=False)
            targets=torch.autograd.Variable(y[b], requires_grad=False)

            #Train
            hiddenOut = self.model[idx].forwardToHidden(points)
            self.optimizer[idx].train(inputs=hiddenOut, targets=targets)

            # Forward
            #outputs=self.model.forward(points)
            #import pdb;pdb.set_trace()
            # loss=self.criterion(outputs,targets)
            #
            # # for i, j in zip(targets, outputs):
            # #     print(i.data[0], j.data[0])
            # # import pdb;
            # # pdb.set_trace()
            #
            # # Backward
            # self.optimizer.zero_grad()
            # loss.backward()
            # torch.nn.utils.clip_grad_norm_(self.model.parameters(),self.clipgrad)
            # self.optimizer.step()

        return

    def eval(self, x, y, idx):
        with torch.no_grad():
            # total_loss = 0
            # total_acc = 0
            # total_num = 0
            self.model[idx].eval()

            r = np.arange(x.size(0))
            r = torch.LongTensor(r).cuda()

            self.sbatch = x.size(0)

            #pred = []
            correct=0
            # Loop batches
            for i in range(0, len(r), self.sbatch):
                if i + self.sbatch <= len(r):
                    b = r[i:i + self.sbatch]
                else:
                    b = r[i:]
                points = torch.autograd.Variable(x[b])
                targets = torch.autograd.Variable(y[b])

                # Forward
                output = self.model[idx].forward(points)
                # loss = self.criterion(output, targets)
                #pred.append(output.data.cpu().numpy())
                # # _, pred = output.max(1)
                # # import pdb;
                # # pdb.set_trace()
                # # hits = (pred == targets.type(torch.LongTensor).cuda()).float()
                #
                # total_loss += float(loss.data.cpu().numpy() * len(b))
                # # total_acc += float(hits.sum().data.cpu().numpy())
                # total_num += len(b)
                pred = output.data.max(1)[1]
                # correct += pred.eq(targets.data.long()).cpu().sum()
                # import pdb;
                # pdb.set_trace()

            # import pdb;pdb.set_trace()
            # temp = np.stack(pred[:-1], axis=0)
            # temp = np.append(temp, pred[-1])
            # pred = None
            #
            # return 0, self.score_func(temp > 0.5, y.data.cpu().numpy())
            return 0, self.score_func(pred, y.data.cpu().numpy())

    def predict(self,x):
        with torch.no_grad():
            for e in range(self.nepochs):
                self.model[e].eval()

            r=np.arange(x.size(0))
            r=torch.LongTensor(r).cuda()

            self.sbatch = x.size(0)

            #pred=[]
            correct = 0
            # Loop batches
            for i in range(0,len(r),self.sbatch):
                if i+self.sbatch<=len(r): b=r[i:i+self.sbatch]
                else: b=r[i:]
                points=torch.autograd.Variable(x[b])

                preds=[]
                for e in range(self.nepochs):
                    # Forward
                    output=self.model[e].forward(points)
                    #pred.append(output.data.cpu().numpy())
                    pred = output.data.max(1)[1]
                    preds.append(pred.unsqueeze(1))
                    #correct += pred.eq(targets.data.long()).cpu().sum()

                    # temp = np.stack(pred[:-1], axis=0)
                    # temp = np.append(temp, pred[-1])
                    # pred=None

                preds = torch.cat(preds, dim=1).cpu().numpy()
                ensembled = []
                for ans in preds:
                    ensembled.append(int(np.bincount(ans).argmax()))
                ensembled = torch.LongTensor(ensembled).cuda()

            return ensembled