import sys,os,argparse
import numpy as np
import scipy
import pandas as pd
import sklearn
from sklearn.metrics import roc_curve, precision_recall_curve, auc, make_scorer, recall_score, accuracy_score, precision_score, confusion_matrix, f1_score, average_precision_score
from scipy.stats.stats import pearsonr
from scipy.spatial import distance
from scipy.stats import wilcoxon
from scipy.stats import ttest_ind
from scipy.stats import chi2_contingency
from sklearn.feature_selection import VarianceThreshold
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
from matplotlib import patches
import utils
import seaborn as sns



###############################################################################
############ collect results in a structure
###############################################################################
def collect_results(experiments,approaches,samples,kfolds,seeds,encodings,explain,data_path,resdir,fun_interpret):
    df=None
    buffer = ""
    metrics = {}
    for sample in samples:
        metrics[sample] = {}
        amatrix = []
        datatest = []
        for experiment in experiments:
            metrics[sample][experiment] = {}

            #read config file for the experiment from the experiments directory
            config = utils.load_config(experiment=experiment)
            from dataloaders import dataset as dataloader
            _, protected,kfold=dataloader.get(filename=config.get('dataset', 'filename'),url=config.get('dataset', 'url'),label_column=config.get('experiment', 'label_column'),
                protected_features=config.get('experiment', 'protected_features'),filename_test=config.get('dataset', 'filename_test'),root=data_path,code_numerical=config.get('experiment', 'code_numerical'),
                drop_train=config.get('experiment', 'drop_train'),drop_columns=config.get('experiment', 'drop_columns'),group_column=config.get('experiment', 'group_column'),column_id=config.get('experiment', 'column_id'),
                experiment=experiment,encoding=encodings[0],idfold=0,kfold=kfolds,seed=0)
            #maybe this should be set by args instead of loading it from the config file
            metrics[sample][experiment]['protected'] = protected

            for k in range(kfolds,kfolds+1):
                metrics[sample][experiment][k] = {}
                for encoding in encodings:
                    metrics[sample][experiment][k][encoding] = {}
                    save_df_approach = []
                    for approach in approaches:
                        metrics[sample][experiment][k][encoding][approach] = {}
                        save_df_seed = []
                        for seed in seeds:
                            #collect data frames with results
                            df, explain_df, categ_df = collect_df(experiment,approach,sample,k,seed,encoding,explain,data_path,resdir,fun_interpret)
                            #compute metrics
                            metrics[sample][experiment][k][encoding][approach][seed],df_test = collect_metrics(df, explain_df, categ_df, protected, experiment,approach,sample,k,seed,encoding,explain,data_path,resdir,fun_interpret)

                            datatest.append(df_test)
                            if explain:
                                save_df_seed.append(explain_df)

                        if explain:
                            save_df_approach.append(save_df_seed)

                        if explain:
                            acmatrix = np.zeros((len(seeds),int(len(approaches)*(len(approaches)-1)/2),save_df_approach[0][0].shape[0]))
                            for s in range(len(seeds)):
                                ik=0
                                for ia in range(len(approaches)-1):
                                    for ja in range(ia+1,len(approaches)):
                                        cc = np.corrcoef(save_df_approach[ia][s],save_df_approach[ja][s])
                                        acmatrix[s,ik,:]=np.diagonal(cc[:save_df_approach[0][0].shape[0],save_df_approach[0][0].shape[0]:])
                                        #import pdb;pdb.set_trace()
                                        ik += 1
                            amatrix.append(acmatrix)
                            # scmatrix = np.zeros((len(approaches),int(len(seeds)*(len(seeds)-1)/2),save_df_approach[0][0].shape[0]))
                            # for i,approach in enumerate(approaches):
                            #     ik=0
                            #     for si in range(len(approaches)-1):
                            #         for sj in range(si+1,len(approaches)):
                            #             cc = np.corrcoef(save_df_approach[i][si],save_df_approach[i][sj])
                            #             scmatrix[i,ik]=np.diagonal(cc[:save_df_approach[0][0].shape[0],save_df_approach[0][0].shape[0]:])
                            #             #import pdb;pdb.set_trace()
                            #             ik += 1

    return metrics, datatest, amatrix


def collect_df(experiment,approach,sample,k,seed,encoding,explain,data_path,resdir,fun_interpret):
    metrics = {'acc': [],'auc': [],'p': [],'r': [],'avp': [],'fpr': [],'tpr': [],'roc': [],'fair': []}

    list_df = []
    list_df_valid = []
    list_explain_df = []
    list_categ_df = []

    for fold in range(k):
        if 'vanilla' in approach:
            pkl_file = os.path.join(resdir,'exp1_savry' + '_'+ 'logit' + '_' + str(encoding)+ '_' + str(k)+ '_' + str(fold)  + '_' + str(0) + '_1_scores.pkl')
            pkl_file_valid = os.path.join(resdir,'exp1_savry' + '_'+ 'logit' + '_' + str(encoding)+ '_' + str(k)+ '_' + str(fold) + '_' + str(0) + '_1_valid_scores.pkl')
        else:
            pkl_file=os.path.join(resdir,experiment+'_'+approach+'_'+str(encoding)+ '_' + str(k)+ '_' + str(fold)+'_'+str(seed)+'_'+str(sample)+'_scores.pkl')
            pkl_file_valid = os.path.join(resdir,experiment + '_' + approach + '_' + str(encoding)+ '_' + str(k)+ '_' + str(fold) + '_' + str(seed)+'_'+str(sample) + '_valid_scores.pkl')
        temp_df = pd.read_pickle(pkl_file)
        temp_df_valid = pd.read_pickle(pkl_file)

        #filter on group if group exists for this experiment: used in filtering savry people from demog_4000
        if 'group' in temp_df:
            filtercon = temp_df['group'] == 1
            temp_df=temp_df.loc[filtercon]

        #this deals with non-ML methods, like compas score and savry score which are taken from the features
        if 'vanilla' in approach:
            if 'savry' in approach:
                if 'sum' in approach:
                    temp_df['score_prob'] = (temp_df['V60_SAVRY_total_score'] - temp_df['V60_SAVRY_total_score'].min()) / (
                        temp_df['V60_SAVRY_total_score'].max() - temp_df['V60_SAVRY_total_score'].min())
                    temp_df_valid['score_prob'] = (temp_df_valid['V60_SAVRY_total_score'] - temp_df_valid[
                        'V60_SAVRY_total_score'].min()) / (temp_df_valid['V60_SAVRY_total_score'].max() - temp_df_valid[
                                                    'V60_SAVRY_total_score'].min())
                else:
                    temp_df['score_prob'] = temp_df['V56_@R1_resum_risc_global_reverse']
                    temp_df_valid['score_prob'] = temp_df_valid['V56_@R1_resum_risc_global_reverse']
            elif 'decile_score.1' in temp_df.columns:
                temp_df['score_prob'] = temp_df['decile_score.1'] / 10
                temp_df_valid['score_prob'] = temp_df_valid['decile_score.1'] / 10

        ###############explanations - interpretability
        if explain:
            if "vanilla" not in approach:
                pkl_file=os.path.join(resdir,experiment+'_'+approach+'_'+str(encoding)+ '_' + str(k)+ '_' + str(fold)+'_'+str(seed)+'_'+str(sample)+'_explains.pkl')
                ex_scores_df = pd.read_pickle(pkl_file)
                # filter on group if group exists for this experiment: used in filtering savry people from demog_4000
                if 'group' in temp_df:
                    ex_scores_df = ex_scores_df.loc[filtercon.values]
                list_explain_df.append(ex_scores_df)
                pkl_file = os.path.join(resdir,experiment + '_' + approach + '_' + str(encoding) + '_' + str(k) + '_' + str(fold) + '_' + str(seed) + '_' + str(sample) + '_explainc.pkl')
                ex_categ_df = pd.read_pickle(pkl_file)
                if 'group' in temp_df:
                    ex_categ_df = ex_categ_df.loc[filtercon.values]
                list_categ_df.append(ex_categ_df)

        fpr_valid, tpr_valid, auc_thresholds_valid = roc_curve(temp_df_valid['label_value'], temp_df_valid['score_prob'])
        fpr_valid = fpr_valid[auc_thresholds_valid <= 1]
        tpr_valid = tpr_valid[auc_thresholds_valid <= 1]
        auc_thresholds_test = auc_thresholds_valid[auc_thresholds_valid <= 1]
        # ROC=utils.compute_roc(train_scores,y[:split1],len(y[:split1]))
        # BA = 0.5*(ROC[:,1]+ROC[:,3])
        thresh_arr_best_ind = np.where((tpr_valid + 1 - fpr_valid) == np.max(tpr_valid + 1 - fpr_valid))[0][0]
        thresh_arr_best = np.array(auc_thresholds_test)[thresh_arr_best_ind]
        #print("best thresh "+approach+": "+str(thresh_arr_best))

        if 'vanilla savry expert' in approach:
            temp_df['score'] = temp_df['score_prob'] > 0.5
        else:
            temp_df['score'] = temp_df['score_prob'] > thresh_arr_best
        temp_df['threshold'] = thresh_arr_best.astype(np.float32)
        temp_df_valid['score'] = temp_df_valid['score_prob'] > thresh_arr_best
        temp_df_valid['threshold'] = thresh_arr_best.astype(np.float32)
        list_df.append(temp_df)
        list_df_valid.append(temp_df_valid)

    #join the folds
    temp_df=None
    df=pd.concat(list_df,sort=False)
    print(df.shape)

    # #SAVRY translation from catalan - for plotting reasons
    if 'exp1' in experiment:
        df.replace(['Dona', 'Home'], ['Female', 'Male'], inplace=True)
        df.replace(['Estranger','Espanyol'], ['Foreigner','Spanish'], inplace=True)
        df.replace(['Magrib', 'Altres','Europa', 'Centre i Sud Amèrica'], ['Maghrebi', 'Other','European', 'Latin American'], inplace=True)

    if explain:
        ####explainability - interpretability
        list_df=None
        if "vanilla" not in approach:
            ex_scores_df = None
            explain_df = pd.concat(list_explain_df, sort=False)
            print(explain_df.shape)
            list_explain_df = None
            ex_categ_df = None
            categ_df = pd.concat(list_categ_df, sort=False)
            print(categ_df.shape)
            list_categ_df = None
        print('data loaded...')
        for column in categ_df.columns:
            if len(categ_df[column].unique())>5 and (any('>' in c for c in categ_df[column].unique()) or any('>' in c for c in categ_df[column].unique())):
                categ_df[column] = column+"="+pd.qcut(df[column], q=4,duplicates='drop').astype(str)
    if explain:
        return df, explain_df, categ_df
    else:
        return df, None, None



def collect_metrics(df, explain_df, categ_df, protected, experiment,approach,sample,k,seed,encoding,explain,data_path,resdir,fun_interpret):
    metrics = {'acc': [],'auc': [],'p': [],'r': [],'avp': [],'fpr': [],'tpr': [],'roc': [],'fair': []}

    #compute measures
    print("Results for '{}' dataset, using '{}', with '{}' encoding:".format(experiment, approach,encoding))
    buffer = "Results for '{}' dataset, using '{}', with '{}' encoding \n".format(experiment, approach,encoding)
    acc_test = accuracy_score(df['label_value'], df['score'])
    print("Accuracy on test: {:.4%}".format(acc_test))
    # bacc_test = balanced_accuracy_score(df['label_value'], df['score'])
    # print("Balanced Accuracy on test: {:.4%}".format(acc_test))
    metrics['acc']=acc_test

    print('Confusion matrix:')
    confusionm_test = confusion_matrix(df['label_value'], df['score'])
    print(pd.DataFrame(confusionm_test, columns=['pred_neg', 'pred_pos'], index=['neg', 'pos']))
    fpr_test, tpr_test, auc_thresholds_test = roc_curve(df['label_value'], df['score_prob'], drop_intermediate=False)

    auc_test = auc(fpr_test, tpr_test)
    print("Area under the curve for test: {:5.4f}".format(auc_test))  # AUC of ROC
    buffer += "Area under the curve on test: {:5.4f} \n".format(auc_test)
    metrics['auc'] = auc_test
    metrics['fpr'] = fpr_test
    metrics['tpr'] = tpr_test

    p_test, r_test, _ = precision_recall_curve(df['label_value'], df['score_prob'])
    avp_test = average_precision_score(df['label_value'], df['score_prob'])
    print('Average precision-recall score test: {0:0.2f}'.format(avp_test))
    #utils.plot_pr_curve(p_test,r_test, avp_test)
    buffer += 'Average precision-recall score test: {0:0.2f} \n'.format(avp_test)
    metrics['p'] = p_test
    metrics['r'] = r_test
    metrics['avp'] = avp_test

    ROC_temp = utils.compute_roc(df['score_prob'], df['label_value'], df['threshold'], len(df))
    metrics['roc'] = ROC_temp

    ROC_temp=utils.compute_roc_thresholds(df['score_prob'],df['label_value'],len(df))
    metrics['roc_thresholds'] = ROC_temp

    ###################### run ttests or chisquare tests to determine baselines differences
    selected_columns = np.setdiff1d([column for column in df.columns],np.array(['label_value','score_prob','score','threshold']))
    df_test = pd.DataFrame(columns=selected_columns)
    df_testp = pd.DataFrame(columns=selected_columns)
    listt=[]
    listp=[]
    listb=[]
    textt=[]
    for column in selected_columns:
        if column not in ['label_value','score_prob','score','threshold']:
            if not np.issubdtype(df[column].dtype, np.number) or (np.issubdtype(df[column].dtype, np.number) and len(df[column].unique())<=6 and df[column].value_counts().min()>=5):
                result = [[sum((df['label_value'] == cat1) & (df[column] == cat2))
                            for cat2 in df[column].unique()]
                            for cat1 in range(2)]
                stattest = chi2_contingency(result)
                listb.append(0)
                textt.append("0:\n"+df[df['label_value']==0][column].value_counts().to_string()+"\n 1:\n"+df[df['label_value']==1][column].value_counts().to_string())
            else:
                stattest = ttest_ind(df[df['label_value']==0][column],df[df['label_value']==1][column])
                listb.append(1)
                textt.append("0: mean="+str(df[df['label_value']==0][column].mean())+", std="+str(df[df['label_value']==0][column].mean())+", 1: mean="+str(df[df['label_value']==1][column].mean())+", std="+str(df[df['label_value']==1][column].std()))
            listt.append(stattest[0])
            listp.append(stattest[1])
    df_test.loc[0] = listt
    df_test.loc[1] = listp
    df_test.loc[2] = listb
    df_test.loc[3] = textt

    ####explainability - interpretability global metrics
    if explain:
        if 'vanilla' not in approach:
            #explainability: feature importance
            metrics['feature_importance'] = {}
            for fun in fun_interpret.keys():
                metrics['feature_importance'][fun] = {}
                metrics['feature_importance'][fun]['total'] = fun_interpret[fun](explain_df,categ_df,explain_df.shape[0])
                condition = np.logical_and(df['label_value']==1,df['score']==1).values
                if any(condition):
                    metrics['feature_importance'][fun]['tp'] = fun_interpret[fun](explain_df.loc[condition],categ_df.loc[condition], np.sum(condition))
                else:
                    metrics['feature_importance'][fun]['tp'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']), index=metrics['feature_importance'][fun]['total'].index)
                condition = np.logical_and(df['label_value'] == 0, df['score'] == 1).values
                if any(condition):
                    metrics['feature_importance'][fun]['fp'] = fun_interpret[fun](explain_df.loc[condition],categ_df.loc[condition], np.sum(condition))
                else:
                    metrics['feature_importance'][fun]['fp'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']), index=metrics['feature_importance'][fun]['total'].index)

                condition = np.logical_and(df['label_value'] == 1, df['score'] ==0).values
                if any(condition):
                    metrics['feature_importance'][fun]['fn'] = fun_interpret[fun](explain_df.loc[condition],categ_df.loc[condition], np.sum(condition))
                else:
                    metrics['feature_importance'][fun]['fn'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']), index=metrics['feature_importance'][fun]['total'].index)
                condition = np.logical_and(df['label_value'] == 0, df['score'] == 0).values
                if any(condition):
                    metrics['feature_importance'][fun]['tn'] = fun_interpret[fun](explain_df.loc[condition],categ_df.loc[condition], np.sum(condition))
                else:
                    metrics['feature_importance'][fun]['tn'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']), index=metrics['feature_importance'][fun]['total'].index)

                if len(df_test.loc[1])==len(metrics['feature_importance'][fun]['total']):
                    metrics['feature_importance'][fun]['total_p'] = \
                        metrics['feature_importance'][fun]['total'].loc[df_test.loc[1]<0.001].mean() - \
                        metrics['feature_importance'][fun]['total'].loc[df_test.loc[1]>=0.001].mean()
                    metrics['feature_importance'][fun]['tp_p'] = \
                        metrics['feature_importance'][fun]['tp'].loc[df_test.loc[1]<0.001].mean() - \
                        metrics['feature_importance'][fun]['tp'].loc[df_test.loc[1]>=0.001].mean()
                    metrics['feature_importance'][fun]['fp_p'] = \
                        metrics['feature_importance'][fun]['fp'].loc[df_test.loc[1]<0.001].mean() - \
                        metrics['feature_importance'][fun]['fp'].loc[df_test.loc[1]>=0.001].mean()
                    metrics['feature_importance'][fun]['tn_p'] = \
                        metrics['feature_importance'][fun]['tn'].loc[df_test.loc[1]<0.001].mean() - \
                        metrics['feature_importance'][fun]['tn'].loc[df_test.loc[1]>=0.001].mean()
                    metrics['feature_importance'][fun]['fn_p'] = \
                        metrics['feature_importance'][fun]['fn'].loc[df_test.loc[1]<0.001].mean() - \
                        metrics['feature_importance'][fun]['fn'].loc[df_test.loc[1]>=0.001].mean()


    ############## group-wise metrics data gathering
    ntotal=len(df)
    metrics['fair'] = {}
    for protected_feature in range(len(protected)):
        pf = protected[protected_feature]
        metrics['fair'][pf] = {}
        groups = list(df[protected[protected_feature]].unique())

        for p in groups:
            metrics['fair'][pf][p] = {}
            sortcon=np.array((df[protected[protected_feature]] == p).values)
            df_temp = df.loc[sortcon]

            temp_roc = utils.compute_roc(df_temp['score_prob'], df_temp['label_value'],df_temp['threshold'], ntotal)
            metrics['fair'][pf][p]['roc'] = temp_roc

            temp_roc=utils.compute_roc_thresholds(df_temp['score_prob'],df_temp['label_value'],ntotal)
            metrics['fair'][pf][p]['roc_thresholds'] = temp_roc

            fpr_test_g, tpr_test_g, auc_thresholds_test = roc_curve(df_temp['label_value'], df_temp['score_prob'])
            auc_test_g = auc(fpr_test_g, tpr_test_g)
            metrics['fair'][pf][p]['auc'] = auc_test_g

            if explain:
                ####explainability - interpretability
                if 'vanilla' not in approach:
                    # explainability: group-wise feature importance
                    explain_df_temp = explain_df.loc[sortcon]
                    categ_df_temp = categ_df.loc[sortcon]

                    metrics['fair'][pf][p]['feature_importance'] = {}
                    for fun in fun_interpret.keys():
                        metrics['fair'][pf][p]['feature_importance'][fun] = {}
                        metrics['fair'][pf][p]['feature_importance'][fun]['total'] = fun_interpret[fun](explain_df_temp,categ_df_temp, explain_df.shape[0])
                        condition = np.array(np.logical_and(df_temp['label_value'] == 1, df_temp['score'] == 1).values)
                        if any(condition):
                            metrics['fair'][pf][p]['feature_importance'][fun]['tp'] = fun_interpret[fun](explain_df_temp.loc[condition],categ_df_temp.loc[condition],np.sum(condition))
                        else:
                            metrics['fair'][pf][p]['feature_importance'][fun]['tp'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']), index= metrics[sample][experiment][k][encoding][approach][ seed]['feature_importance'][fun]['total'].index)
                        condition = np.array(np.logical_and(df_temp['label_value'] == 0, df_temp['score'] == 1).values)
                        if any(condition):
                            metrics['fair'][pf][p]['feature_importance'][fun]['fp'] = fun_interpret[fun](explain_df_temp.loc[condition],categ_df_temp.loc[condition],np.sum(condition))
                        else:
                            metrics['fair'][pf][p]['feature_importance'][fun]['fp'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']), index=metrics['feature_importance'][fun]['total'].index)
                        condition = np.array(np.logical_and(df_temp['label_value'] == 1, df_temp['score'] == 0).values)
                        if any(condition):
                            metrics['fair'][pf][p]['feature_importance'][fun]['fn'] = fun_interpret[fun](explain_df_temp.loc[condition],categ_df_temp.loc[condition],np.sum(condition))
                        else:
                            metrics['fair'][pf][p]['feature_importance'][fun]['fn'] = pd.Series(np.zeros_like( metrics['feature_importance'][fun]['total']),index=metrics['feature_importance'][fun]['total'].index)
                        condition = np.array(np.logical_and(df_temp['label_value'] == 0, df_temp['score'] == 0).values)
                        if any(condition):
                            metrics['fair'][pf][p]['feature_importance'][fun]['tn'] = fun_interpret[fun](explain_df_temp.loc[condition],categ_df_temp.loc[condition],np.sum(condition))
                        else:
                            metrics['fair'][pf][p]['feature_importance'][fun]['tn'] = pd.Series(np.zeros_like(metrics['feature_importance'][fun]['total']),index=metrics['feature_importance'][fun]['total'].index)

                        if len(df_test.loc[1])==len(metrics['fair'][pf][p]['feature_importance'][fun]['total']):
                            metrics['fair'][pf][p]['feature_importance'][fun]['total_p'] = \
                                metrics['fair'][pf][p]['feature_importance'][fun]['total'].loc[df_test.loc[1]<0.001].mean() - \
                                metrics['fair'][pf][p]['feature_importance'][fun]['total'].loc[df_test.loc[1]>=0.001].mean()
                            metrics['fair'][pf][p]['feature_importance'][fun]['tp_p'] = \
                                metrics['fair'][pf][p]['feature_importance'][fun]['tp'].loc[df_test.loc[1]<0.001].mean() - \
                                metrics['fair'][pf][p]['feature_importance'][fun]['tp'].loc[df_test.loc[1]>=0.001].mean()
                            metrics['fair'][pf][p]['feature_importance'][fun]['fp_p'] = \
                                metrics['fair'][pf][p]['feature_importance'][fun]['fp'].loc[df_test.loc[1]<0.001].mean() - \
                                metrics['fair'][pf][p]['feature_importance'][fun]['fp'].loc[df_test.loc[1]>=0.001].mean()
                            metrics['fair'][pf][p]['feature_importance'][fun]['tn_p'] = \
                                metrics['fair'][pf][p]['feature_importance'][fun]['tn'].loc[df_test.loc[1]<0.001].mean() - \
                                metrics['fair'][pf][p]['feature_importance'][fun]['tn'].loc[df_test.loc[1]>=0.001].mean()
                            metrics['fair'][pf][p]['feature_importance'][fun]['fn_p'] = \
                                metrics['fair'][pf][p]['feature_importance'][fun]['fn'].loc[df_test.loc[1]<0.001].mean() - \
                                metrics['fair'][pf][p]['feature_importance'][fun]['fn'].loc[df_test.loc[1]>=0.001].mean()



        #disparity between groups and the reference_group
        counts = df[protected[protected_feature]].value_counts()
        reference_group = counts.idxmax()
        reference_group_idx = groups.index(reference_group)

        for i in range(len(groups)):
            p=groups[i]
            if i != reference_group_idx:
                metrics['fair'][pf][p]['reference'] = 0
                metrics['fair'][pf][p]['disparity'] = \
                    np.divide(np.array(metrics['fair'][pf][p]['roc']), \
                    np.array(metrics['fair'][pf][reference_group]['roc']), \
                    out=np.zeros_like(metrics['fair'][pf][p]['roc']).astype(np.float32), \
                    where=np.array(metrics['fair'][pf][reference_group]['roc']) != 0)
                metrics['fair'][pf][p]['disparity_thresholds'] = \
                    np.divide(np.array(metrics['fair'][pf][p]['roc_thresholds']),\
                    np.array(metrics['fair'][pf][reference_group]['roc_thresholds']), \
                    out=np.zeros_like(metrics['fair'][pf][p]['roc_thresholds']).astype(np.float32), \
                    where=np.array(metrics['fair'][pf][reference_group]['roc_thresholds']) != 0)
                metrics['fair'][pf][p]['best_errorratebal_thresh']= \
                    np.linspace(1,0,101)[np.argmin(((1-metrics['fair'][pf][p]['disparity_thresholds'][:,0] )**2 + \
                    (1-metrics['fair'][pf][p]['disparity_thresholds'][:,2])**2))]
                metrics['fair'][pf][p]['best_errorratebal']= \
                    np.array(metrics['fair'][pf][p]['disparity_thresholds'][[np.argmin(((1-metrics['fair'][pf][p]['disparity_thresholds'][:,0] )**2 + \
                    (1-metrics['fair'][pf][p]['disparity_thresholds'][:,2])**2))],:])

                # explainability: disparity between groups
                if explain:
                    ####explainability
                    if 'vanilla' not in approach:
                        metrics['fair'][pf][p]['disparity_feature_importance_ratio'] = {}
                        metrics['fair'][pf][p]['disparity_feature_importance_sum'] = {}
                        metrics['fair'][pf][p]['disparity_feature_importance_dif'] = {}
                        #feature disparity
                        for fun in fun_interpret.keys():
                            metrics['fair'][pf][p]['disparity_feature_importance_ratio'][fun] = {}
                            metrics['fair'][pf][p]['disparity_feature_importance_sum'][fun] = {}
                            metrics['fair'][pf][p]['disparity_feature_importance_dif'][fun] = {}
                            for measure in ['total','tp','fp','fn','tn']:
                                metrics['fair'][pf][p]['disparity_feature_importance_ratio'][fun][measure] = metrics['fair'][pf][p]['feature_importance'][fun][measure] / metrics['fair'][pf][reference_group]['feature_importance'][fun][measure]
                                metrics['fair'][pf][p]['disparity_feature_importance_sum'][fun][measure] = np.sum(np.abs(metrics['fair'][pf][p]['feature_importance'][fun][measure] - metrics['fair'][pf][reference_group]['feature_importance'][fun][measure]))
                                metrics['fair'][pf][p]['disparity_feature_importance_dif'][fun][measure] = np.abs(metrics['fair'][pf][p]['feature_importance'][fun][measure] -metrics['fair'][pf][reference_group]['feature_importance'][fun][measure]).dropna()
                            if len(df_test.loc[1])==len(metrics['fair'][pf][p]['feature_importance'][fun]['total']):
                                for measure in ['total_p','tp_p','fp_p','fn_p','tn_p']:
                                    metrics['fair'][pf][p]['disparity_feature_importance_ratio'][fun][measure] = metrics['fair'][pf][p]['feature_importance'][fun][measure] / metrics['fair'][pf][reference_group]['feature_importance'][fun][measure]
                                    metrics['fair'][pf][p]['disparity_feature_importance_sum'][fun][measure] = np.sum(np.abs(metrics['fair'][pf][p]['feature_importance'][fun][measure] - metrics['fair'][pf][reference_group]['feature_importance'][fun][measure]))
                                    metrics['fair'][pf][p]['disparity_feature_importance_dif'][fun][measure] = np.abs(metrics['fair'][pf][p]['feature_importance'][fun][measure] -metrics['fair'][pf][reference_group]['feature_importance'][fun][measure])
            else:
                metrics['fair'][pf][p]['reference'] = 1
                metrics['fair'][pf][p]['disparity'] = \
                    np.ones_like(metrics['fair'][pf][p]['roc']).astype(np.float32)
                metrics['fair'][pf][p]['disparity_thresholds'] = \
                    np.ones_like(metrics['fair'][pf][p]['roc_thresholds']).astype(np.float32)
                metrics['fair'][pf][p]['best_errorratebal_thresh']= 0.5
                metrics['fair'][pf][p]['best_errorratebal']= \
                    np.ones_like(np.transpose(metrics['fair'][pf][p]['roc'])).astype(np.float32)

                if explain:
                    ####explainability
                    if 'vanilla' not in approach:
                        metrics['fair'][pf][p]['disparity_feature_importance_ratio'] = {}
                        metrics['fair'][pf][p]['disparity_feature_importance_sum'] = {}
                        metrics['fair'][pf][p]['disparity_feature_importance_dif'] = {}
                        for fun in fun_interpret.keys():
                            metrics['fair'][pf][p]['disparity_feature_importance_ratio'][fun] = {}
                            metrics['fair'][pf][p]['disparity_feature_importance_sum'][fun] = {}
                            metrics['fair'][pf][p]['disparity_feature_importance_dif'][fun] = {}
                            for measure in ['total', 'tp', 'fp', 'fn', 'tn']:
                                metrics['fair'][pf][p]['disparity_feature_importance_ratio'][measure] = np.ones_like(metrics['fair'][pf][p]['feature_importance'][fun][measure])
                                metrics['fair'][pf][p]['disparity_feature_importance_sum'][fun][measure] = 0
                                metrics['fair'][pf][p]['disparity_feature_importance_dif'][fun][measure] = np.zeros_like(metrics['fair'][pf][p]['feature_importance'][fun][measure])
                            if len(df_test.loc[1])==len(metrics['fair'][pf][p]['feature_importance'][fun]['total']):
                                for measure in ['total_p', 'tp_p', 'fp_p', 'fn_p', 'tn_p']:
                                    metrics['fair'][pf][p]['disparity_feature_importance_ratio'][measure] = np.ones_like(metrics['fair'][pf][p]['feature_importance'][fun][measure])
                                    metrics['fair'][pf][p]['disparity_feature_importance_sum'][fun][measure] = 0
                                    metrics['fair'][pf][p]['disparity_feature_importance_dif'][fun][measure] = np.zeros_like(metrics['fair'][pf][p]['feature_importance'][fun][measure])

    return metrics,df_test
