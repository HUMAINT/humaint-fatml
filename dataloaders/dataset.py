import os,sys
import numpy as np
import torch
import torch.utils.data
import pandas as pd
from sklearn.model_selection import train_test_split,StratifiedKFold
import sklearn.preprocessing
from copy import deepcopy
import utils


#############################################################################################################################################

class dataset(torch.utils.data.Dataset):
    """https://archive.ics.uci.edu/ml/datasets/Adult

    Args:
        root (string): Root directory of dataset where directory ``Traffic signs`` exists.
        split (string): One of {'train', 'test'}.
        transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.RandomCrop``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        download (bool, optional): If true, downloads the dataset from the internet and puts it in root directory.
            If dataset is already downloaded, it is not downloaded again.

    """

    def __init__(self, root, filename,url,label_column,protected_features,filename_test=None,drop_train=None,drop_columns=None,group_column=None,code_numerical=None,column_id=None,train=True,transform=None,download=False,encoding='onehot',valid=False,split=0.1,kfold=0,idfold=0,seed=0,sample=1):
        self.seed=seed
        np.random.seed(self.seed)
        self.root = os.path.expanduser(root)
        self.transform = transform
        self.idfold=idfold
        self.kfold=kfold
        self.seed=seed
        self.sample=sample
        self.filename = filename
        self.filename_test = filename_test
        self.url = url
        self.label_column = label_column
        self.protected_features = protected_features
        self.drop_train = drop_train
        self.drop_columns = drop_columns
        self.group_column = group_column
        self.code_numerical = code_numerical
        self.column_id = column_id
        #import pdb;pdb.set_trace()
        assert idfold < self.kfold, "idfold can't be larger than the number of folds kfold"
        # self.filename = "adult.csv"
        # self.filename_test = "adult.test.csv"
        # self.url = "https://github.com/algofairness/BlackBoxAuditing/blob/master/BlackBoxAuditing/test_data/adult.csv?raw=true"
        # self.label_column='income-per-year'
        self.load(self.label_column,download=download,train=train,encoding=encoding,valid=valid,split=split,kfold=kfold,idfold=idfold,seed=seed,sample=sample)


    def prepare(self,df):
        if self.code_numerical is not None and len(self.code_numerical) > 0:
            cols = [c for c in self.code_numerical if c in df.columns]
            for col in cols:
                vals = df[col].unique()
                df[col].replace(vals, list(range(len(vals))), inplace=True)
        if self.drop_columns is not None and len(self.drop_columns) > 0:
            cols = [c for c in self.drop_columns if c in df.columns]
            df.drop(cols, axis=1, inplace=True)
        return df


    def load(self,label_column,download=False,drop_columns=[],train=True,encoding='onehot',valid=False,split=0.1,kfold=0,idfold=0,seed=0,sample=1):
        if train:
            if not os.path.isfile(os.path.join(self.root, self.filename)):
                if not download:
                   raise RuntimeError('Dataset not found. You can use download=True to download it')
                else:
                    print('Downloading from '+self.url)
                    self.download()
        else:
            if self.filename_test is not None:
                if not os.path.isfile(os.path.join(self.root, self.filename_test)):
                    if not download:
                       raise RuntimeError('Dataset not found. You can use download=True to download it')
                    else:
                        print('Downloading from '+self.url)
                        self.download()

        if train:
            fname = self.filename
        elif self.filename_test is not None:
            fname = self.filename_test
        else:
            fname = self.filename

        with open(os.path.join(self.root,fname),'rb') as f:
            df = pd.read_csv(f)

            #shuffle and reset
            df = df.sample(frac=1,random_state=seed).reset_index(drop=True)

            #remove unwanted columns
            df = self.prepare(df)

            assert len(df[label_column].unique()) == 2, "label column must be binary: %r" % label_column

            #pandas stuff
            #deal with missing data
            df.select_dtypes(include='object',exclude='number').replace({'': np.nan},inplace=True)
            # remove columns with 20% missing data
            df = df.dropna(thresh=0.2 * len(df), axis=1)
            # remove rows with missing data
            #import pdb;pdb.set_trace()
            df = df.dropna()

            #prepare for aequitas
            # self.df=deepcopy(df)
            # self.df.rename(columns={label_column: 'label_value'}, inplace=True)

            # class imbalance
            counts = np.array(df[label_column].value_counts().tolist())
            #weights = 1./sklearn.preprocessing.normalize(counts[:,np.newaxis], axis=0).ravel()

            if self.filename_test is None:
                if kfold>1:
                    skf = StratifiedKFold(n_splits=kfold)
                    #http://scikit-learn.org/stable/auto_examples/model_selection/plot_cv_indices.html#sphx-glr-auto-examples-model-selection-plot-cv-indices-py
                    if self.column_id is not None:
                        df_patients = df.groupby([self.column_id]).first()
                        for i,idx in enumerate(skf.split(np.zeros(len(df_patients[label_column])), df_patients[label_column])):
                            (train_index, test_index) = idx
                            train_index = np.take(df_patients.index.values,train_index)
                            test_index = np.take(df_patients.index.values,test_index)
                            if i==idfold:
                                dfx_train = df.loc[df[self.column_id].isin(train_index)]
                                dfx_test = df.loc[df[self.column_id].isin(test_index)]
                                dfy_train = dfx_train[label_column]
                                dfy_test = dfx_test[label_column]
                                dfx_train = dfx_train[dfx_train.columns.drop(label_column)]
                                dfx_test = dfx_test[dfx_test.columns.drop(label_column)]
                    else:
                        for i,idx in enumerate(skf.split(np.zeros(len(df[label_column])), df[label_column])):
                            (train_index, test_index) = idx
                            if i==idfold:
                                dfx_train = df.iloc[train_index]
                                dfx_test = df.iloc[test_index]
                                dfy_train = dfx_train[label_column]
                                dfy_test = dfx_test[label_column]
                                dfx_train = dfx_train[dfx_train.columns.drop(label_column)]
                                dfx_test = dfx_test[dfx_test.columns.drop(label_column)]
                else:
                    dfx_train, dfx_test, dfy_train, dfy_test = train_test_split(df[df.columns.drop(label_column)],df[label_column], test_size=split)
                if train:
                    self.dfx = dfx_train
                    self.dfy = dfy_train
                else:
                    self.dfx = dfx_test
                    self.dfy = dfy_test
            else:
                self.dfx = deepcopy(df[df.columns.drop(label_column)])
                self.dfy = deepcopy(df[label_column])

            #import pdb;pdb.set_trace()
            if train:
                if sample<1:
                    dfx_train, _, dfy_train, _ = train_test_split(self.dfx, self.dfy, test_size=1.-sample)
                    self.dfx = dfx_train
                    self.dfy = dfy_train

                if self.column_id is not None:
                    dfx_train[label_column]=dfy_train
                    df_patients = dfx_train.groupby([self.column_id]).first()
                    _, _, t_train, t_valid= train_test_split(np.zeros(len(df_patients.index)), df_patients[label_column], test_size=0.1,stratify=df_patients[label_column])
                    #import pdb;pdb.set_trace()
                    train_index = t_train.index.values
                    valid_index = t_valid.index.values
                    dfx_train_ = dfx_train.loc[dfx_train[self.column_id].isin(train_index)]
                    dfx_valid = dfx_train.loc[dfx_train[self.column_id].isin(valid_index)]
                    dfy_train_ = dfy_train.loc[dfx_train[self.column_id].isin(train_index)]
                    dfy_valid = dfy_train.loc[dfx_train[self.column_id].isin(valid_index)]
                    dfx_train = dfx_train_
                    dfx_train_ = None
                    dfy_train = dfy_train_
                    dfy_train_ = None
                    dfx_valid.drop(label_column,axis=1,inplace=True)
                    dfx_train.drop(label_column,axis=1,inplace=True)
                    #import pdb;pdb.set_trace()
                else:
                    dfx_train, dfx_valid, dfy_train, dfy_valid= train_test_split(self.dfx, self.dfy, test_size=0.1)
                if not valid:
                    self.dfx = dfx_train
                    self.dfy = dfy_train
                else:
                    self.dfx = dfx_valid
                    self.dfy = dfy_valid

    def getweights(self):
        return self.weights

    def getdf(self):
        return self.dfx,self.dfy

    def getmapper(self):
        return self.mapper

    def __getitem__(self, index):
        """
        Args: index (int): Index
        Returns: tuple: (point, target) where target is index of the target class.
        """
        #point, target, weight = self.data[index], self.labels[index], self.weight[index]
        point, target= self.dfx[index], self.dfy[index]

        return point, target

    def __len__(self):
        return len(self.data)

    def download(self):
        import errno, urllib.request
        root = os.path.expanduser(self.root)

        for fname in [self.filename,self.filename_test]:
            if fname is not None:
                fpath = os.path.join(root, fname)

                try:
                    os.makedirs(root)
                except OSError as e:
                    if e.errno == errno.EEXIST:
                        pass
                    else:
                        raise
                urllib.request.urlretrieve(self.url.replace(self.filename,fname), fpath)

########################################################################################################################

def get(experiment,filename,url,label_column,protected_features,root='../dat/',filename_test=None,drop_train=None,drop_columns=None,group_column=None,code_numerical=None,column_id=None,encoding='onehot',split=0.1,idfold=0,kfold=4,seed=0,sample=1):
    dat={}
    dat['train'] = dataset(filename=filename,url=url,label_column=label_column,protected_features=protected_features,filename_test=filename_test,drop_train=drop_train,drop_columns=drop_columns,group_column=group_column,code_numerical=code_numerical,column_id=column_id,root=root,train=True,download=True,transform=None,encoding=encoding,split=split,idfold=idfold,kfold=kfold,seed=seed,sample=sample)
    dat['test'] = dataset(filename=filename,url=url,label_column=label_column,protected_features=protected_features,filename_test=filename_test,drop_train=drop_train,drop_columns=drop_columns,group_column=group_column,code_numerical=code_numerical,column_id=column_id,root=root,train=False,download=True,transform=None,encoding=encoding,split=split,idfold=idfold,kfold=kfold,seed=seed,sample=sample)
    dat['valid'] = dataset(filename=filename,url=url,label_column=label_column,protected_features=protected_features,filename_test=filename_test,drop_train=drop_train,drop_columns=drop_columns,group_column=group_column,code_numerical=code_numerical,column_id=column_id,root=root, train=True,valid=True, download=True, transform=None, encoding=encoding,split=split,idfold=idfold,kfold=kfold,seed=seed,sample=sample)

    data={}
    data['name']=experiment
    for s in ['train', 'test', 'valid']:
        data[s] = {'x': [], 'y': []}
        data[s]['x'] = dat[s].dfx
        data[s]['y'] = dat[s].dfy
        if dat[s].group_column is not None:
            data[s]['g'] = dat[s].dfx[dat[s].group_column]
            data[s]['x'].drop(dat[s].group_column, axis=1, inplace=True)
        if dat[s].drop_train is not None:
            cols = [c for c in dat[s].drop_train if c in data[s]['x'].columns]
            data[s]['putback'] = dat[s].dfx[cols]
            data[s]['x'].drop(cols, axis=1, inplace=True)

    return data,dat['train'].protected_features,dat['train'].kfold
