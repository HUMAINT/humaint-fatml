import sys,os,argparse,time
import numpy as np
import torch
import scipy
import pandas as pd
import sklearn
from sklearn.metrics import roc_curve, precision_recall_curve, auc, make_scorer, recall_score, accuracy_score, precision_score, confusion_matrix, f1_score, average_precision_score,roc_auc_score
import pickle
import ast
import utils
import matplotlib.pyplot as plt

tstart=time.time()

# Arguments
parser=argparse.ArgumentParser(description='xxx')
parser.add_argument('--seed',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--fold',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--load',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--kfolds',type=int,default=1,help='(default=%(default)d)')
parser.add_argument('--sample',default=1,type=float,required=False,help='(default=%(default)f)')
parser.add_argument('--experiment',default='',type=str,required=True,help='(default=%(default)s)')
parser.add_argument('--approach',default='',type=str,required=True,choices=['mlp_batchfair','mlp','logit','knn','linearsvm','rbfsvm','gaussian','tree','forest','adaboost','naivebayes','boostedtrees'],help='(default=%(default)s)')
parser.add_argument('--nepochs',default=200,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--sbatch',default=64,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--niter',default=30,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--weighted',default=10,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--noparams',default=3e4,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--lr',default=0.001,type=float,required=False,help='(default=%(default)f)')
parser.add_argument('--split',default=0.1,type=float,required=False,help='(default=%(default)f)')
parser.add_argument('--threshold',default=0.5,type=float,required=False,help='(default=%(default)f)')
parser.add_argument('--encoding',default='onehot',type=str,required=False,choices=['onehot','binary','backward','ordinal','helmert','sum','hashing','poly','basen','target','looe'],help='(default=%(default)d)')
parser.add_argument('--parameter',type=str,default='',help='(default=%(default)s)')
parser.add_argument('--output',type=str,default='',help='(default=%(default)s)')
parser.add_argument('--save_scores',default=1,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--interpretability',default=1,type=int,required=False,help='(default=%(default)d)')
parser.add_argument('--data_path',type=str,default='dat',help='(default=%(default)s)')
parser.add_argument('--storeinparent',type=int,default=1,help='(default=%(default)s)')
parser.add_argument('--mitigate',type=int,default=0,help='(default=%(default)s)')
parser.add_argument('--protected_id',type=int,default=0,help='(default=%(default)s)')

#parse args
args=parser.parse_args()
args.approach=args.approach.replace("\r","").replace("\n","")
#get current dir

if args.storeinparent:
    pardir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
else:
    pardir = os.path.dirname(os.path.abspath(__file__))
datdir = os.path.join(pardir,args.data_path)
resdir = os.path.join(pardir,'res')

if args.output=='':
    if args.mitigate>0:
        args.output=os.path.join(resdir,args.experiment+'_'+args.approach+'_'+str(args.encoding)+'_'+str(args.kfolds)+'_'+str(args.fold)+'_'+str(args.seed)+'_'+str(args.sample)+'_'+str(args.mitigate)+'_'+str(args.protected_id)+'.txt')
    else:
        args.output=os.path.join(resdir,args.experiment+'_'+args.approach+'_'+str(args.encoding)+'_'+str(args.kfolds)+'_'+str(args.fold)+'_'+str(args.seed)+'_'+str(args.sample)+'.txt')
args.outfile =  str.replace(args.output,'.txt','.pth.tar')
print(args.output)

os.makedirs(resdir, exist_ok=True)
os.makedirs(datdir, exist_ok=True)
print('='*100)
print('Arguments =')
for arg in vars(args):
    print('\t'+arg+':',getattr(args,arg))
print('='*100)


#read config file for the experiment from the experiments directory
config = utils.load_config(experiment=args.experiment)
protected_features = utils.get_config(config,'experiment', 'protected_features')

# Scorers
scorers = {
    'auc_score': make_scorer(roc_auc_score),
    'accuracy_score': make_scorer(accuracy_score)
}
refit_score='auc_score'

# Seed
np.random.seed(args.seed)
torch.manual_seed(args.seed)
if torch.cuda.is_available(): torch.cuda.manual_seed(args.seed)
else: print('[CUDA unavailable]'); #sys.exit()

######################################################################
from dataloaders import dataset as dataloader

# Load
print('Load data...') #(lambda x:int(x) if x is not None else None)(utils.get_config(config,'experiment', 'equalize_id')), #utils.get_config(config,'experiment', 'equalize_baserates')
data,protected,kfold=dataloader.get(filename=utils.get_config(config,'dataset', 'filename'),url=utils.get_config(config,'dataset', 'url'),label_column=utils.get_config(config,'experiment', 'label_column'),
    equalize_baserates=args.mitigate,equalize_id=args.protected_id,
    protected_features=protected_features,filename_test=utils.get_config(config,'dataset', 'filename_test'),root=datdir,code_numerical=utils.get_config(config,'experiment', 'code_numerical'),
    drop_train=utils.get_config(config,'experiment', 'drop_train'),drop_columns=utils.get_config(config,'experiment', 'drop_columns'),group_column=utils.get_config(config,'experiment', 'group_column'),column_id=utils.get_config(config,'experiment', 'column_id'),
    experiment=args.experiment,encoding=args.encoding,split=args.split,idfold=args.fold,kfold=args.kfolds,seed=args.seed,sample=args.sample)
print(data['train']['x'].columns.values)
#import pdb;pdb.set_trace()
if args.mitigate==2:
    protected_feature = protected_features[args.protected_id]
    if protected_feature not in data['train']['x'].columns:
        sys.exit("protected feature is not in the data")

######################################################################
#train,valid,test splits together so mapping is done correctly
split1=len(data['train']['y'])
split2=len(data['train']['y'])+len(data['valid']['y'])
lb = sklearn.preprocessing.LabelBinarizer()
y= lb.fit_transform(pd.concat([data['train']['y'],data['valid']['y'],data['test']['y']],sort=False)).astype(np.uint8)

data_all = pd.concat([data['train']['x'],data['valid']['x'],data['test']['x']],sort=False)
numerical,binaries,categorical=utils.get_column_type(data_all)
numericalids,binariesids,categoricalids=utils.get_column_type_ids(data_all)

# df = pd.concat([data['train']['x'],data['valid']['x'],data['test']['x']],sort=False)
# df['score_prob1'] = df['V60_SAVRY_total_score'] / 48
# df['label_value'] = pd.concat([data['train']['y'],data['valid']['y'],data['test']['y']],sort=False)
# print(roc_auc_score(df['label_value'],df['score_prob1']))
# import pdb;pdb.set_trace()

######################################################################
## We need this for interpretability in LIME
######################################################################
numerical_features = numerical
categorical_features = binariesids+categoricalids
categorical_features_names = binaries+categorical
categorical_names = {}
for id,feature in zip(categorical_features,categorical_features_names):
    le = sklearn.preprocessing.LabelEncoder()
    le.fit(data_all[feature])
    data_all[feature] = le.transform(data_all[feature])
    categorical_names[id] = le.classes_

#construct vector of protected features
if args.approach=='mlp_batchfair':
    P=[]
    reference_group = []
    groups = []
    for p in protected:
        P.append(data_all[p])
        counts = data_all[p].value_counts()
        reference_group.append(counts.idxmax())
        groups.append(data_all[p].unique())
    P=np.transpose(np.array(P))
    ref=np.array(reference_group)

######################################################################
#categorical to onehot
mapper1= utils.preprocessing(data_all,args.encoding,numerical,binaries,categorical)
#mapper.transformed_names_
X = mapper1.fit_transform(data_all)

if args.mitigate==2:
    protected_feature = protected_features[args.protected_id]
    groups = data_all[protected_feature].unique()
    reference_group = data_all[protected_feature].value_counts().idxmax()
    protected_mapped = mapper1.transformed_names_.index(protected_feature)
    for i in np.unique(X[:,protected_mapped]):
        if np.sum(X[:,protected_mapped]==i)==data_all[protected_feature].value_counts()[reference_group]:
            reference_value = i
    train_sensitive_idx = data_all.iloc[:split1][protected_feature]!=reference_group
    valid_sensitive_idx = data_all.iloc[split1:split2][protected_feature]!=reference_group
    test_sensitive_idx = data_all.iloc[split2:][protected_feature]!=reference_group
    #import pdb;pdb.set_trace()
    learned_model = utils.equalize_other(X[:split1],y[:split1],train_sensitive_idx)
    Xt3,_ = utils.transform_features(X[split2:],y[split2:],test_sensitive_idx,learned_model, threshold=0.5)
    Xt2,_ = utils.transform_features(X[split1:split2],y[split1:split2],valid_sensitive_idx,learned_model, threshold=0.5)
    Xt1,yt1 = utils.transform_features(X[:split1],y[:split1],train_sensitive_idx,learned_model, threshold=0.5)
    X[:split1],y[:split1] = Xt1,yt1
    X[split1:split2] = Xt2
    X[split2:] = Xt3

inputsize=X.shape[-1]
print('Input size =',inputsize)
print('Data size =',X.shape[0])
protected_idx=[[i for i in range(len(mapper1.transformed_names_)) if p in mapper1.transformed_names_[i]] for p in protected]
counts=[(X[:,3]==np.unique(X[:,3])[i]).sum() for i in range(len(np.unique(X[:,3])))]

# from BlackBoxAuditing.repairers.GeneralRepairer import Repairer
# features = data_all.columns.tolist()
# protected_features = utils.get_config(config,'experiment', 'protected_features')
# protected_idx = (lambda x:int(x) if x is not None else None)(utils.get_config(config,'experiment', 'equalize_id'))
# protected_feature = protected_features[protected_idx]
# index = features.index(protected_feature)
# repair_level = 0.5
# #repairer = Repairer(features, index, repair_level, False)
# #repaired_data = repairer.repair(data_all)
# # features = dataset.features.tolist()
# # index = dataset.feature_names.index(self.sensitive_attribute)
# # repairer = self.Repairer(features, index, self.repair_level, False)
# #
# # repaired = dataset.copy()
# # repaired_features = repairer.repair(features)
# # repaired.features = np.array(repaired_features, dtype=np.float64)
# # # protected attribute shouldn't change
# # repaired.features[:, index] = repaired.protected_attributes[:, 0]
#
# datat =[[data_all[column].iloc[i] for i in range(data_all.shape[0])] for column in data_all.columns]
# repairer = Repairer(datat, 1, 0.5, False)
# repaired_data = repairer.repair(datat)
# import pdb;pdb.set_trace()
######################################################################
# Args -- Approach
if args.approach=='mlp':
    from approaches import mlp as approach
    #from skorch import NeuralNetClassifier
elif args.approach=='mlp_batchfair':
    from approaches import mlp_batchfair as approach
elif args.approach=='knn':
    from sklearn.neighbors import KNeighborsClassifier
    appr = KNeighborsClassifier()
    param_grid = dict(n_neighbors=scipy.stats.randint(3, 20), weights=['uniform','distance'],metric=['minkowski','euclidean','manhattan'])
elif args.approach=='logit':
    from sklearn.linear_model import LogisticRegression
    appr = LogisticRegression()
    param_grid = dict(C=scipy.stats.uniform(0.1, 10))
elif args.approach=='linearsvm':
    from sklearn.svm import SVC
    appr = SVC(kernel="linear", probability=True)
    param_grid = dict(C=scipy.stats.uniform(0.1, 10))
elif args.approach=='rbfsvm':
    from sklearn.svm import SVC
    appr = SVC(probability=True)
    param_grid = dict(C=scipy.stats.uniform(0.1, 10), gamma=scipy.stats.uniform(0.1, 10))
elif args.approach=='gaussian':
    from sklearn.gaussian_process import GaussianProcessClassifier
    from sklearn.gaussian_process.kernels import RBF,RationalQuadratic,ExpSineSquared,ConstantKernel,DotProduct,Matern
    appr = GaussianProcessClassifier()#warm_start=True
    args.niter=5
    param_grid = dict(kernel=[1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
                            1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
                            1.0 * ExpSineSquared(length_scale=1.0, periodicity=3.0,
                                                 length_scale_bounds=(0.1, 10.0),
                                                 periodicity_bounds=(1.0, 10.0)),
                            ConstantKernel(0.1, (0.01, 10.0))
                            * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.0, 10.0)) ** 2),
                            1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0),nu=1.5)])
elif args.approach=='tree':
    from sklearn.tree import DecisionTreeClassifier
    appr=DecisionTreeClassifier()
    param_grid = dict(criterion=['gini','entropy'],max_depth=scipy.stats.randint(5, 50),
        min_samples_leaf = scipy.stats.randint(1, 10))
elif args.approach=='forest':
    from sklearn.ensemble import RandomForestClassifier
    appr=RandomForestClassifier(max_features=inputsize)
    param_grid = dict(n_estimators=scipy.stats.randint(10, 50),
        max_depth=scipy.stats.randint(5, 50),min_samples_leaf=scipy.stats.randint(1, 10))
elif args.approach=='adaboost':
    from sklearn.ensemble import AdaBoostClassifier
    from sklearn.tree import DecisionTreeClassifier
    DTC = DecisionTreeClassifier(max_features="auto", max_depth=None)
    appr=AdaBoostClassifier(base_estimator = DTC)
    param_grid = {"base_estimator__criterion": ["gini", "entropy"],
                  "base_estimator__splitter": ["best", "random"],
                  "n_estimators": scipy.stats.randint(3, 50)
                  }
elif args.approach=='naivebayes':
    from sklearn.naive_bayes import GaussianNB
    appr=GaussianNB()
elif args.approach=='qda':
    from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
    appr=QuadraticDiscriminantAnalysis()
elif args.approach == 'boostedtrees':
    from xgboost import XGBClassifier
    appr=XGBClassifier(objective = 'binary:logistic')
    param_grid = {'n_estimators': scipy.stats.randint(150, 1000),
                  'learning_rate': scipy.stats.uniform(0.01, 0.6),
                  'subsample': scipy.stats.uniform(0.3, 0.9),
                  'max_depth': [3, 4, 5, 6, 7, 8, 9],
                  'colsample_bytree': scipy.stats.uniform(0.5, 0.9),
                  'min_child_weight': [1, 2, 3, 4]
                 }

# Args -- Network
if args.approach=='mlp' or args.approach=='mlp_batchfair':
    from networks import mlp as network
if args.approach=='test':
    from networks import test as network

######################################################################
# Inits

if args.approach=='mlp' or args.approach=='mlp_batchfair':
    print('Inits...')
    if torch.cuda.is_available(): net=network.Net(inputsize).cuda()
    else: net=network.Net(inputsize)
    utils.print_model_report(net)

    appr=approach.Appr(net,score_func=roc_auc_score,outfile=args.outfile,nepochs=args.nepochs,lr=args.lr,sbatch=args.sbatch,args=args,weights=None)
    print('-'*100)
    if args.load:
        assert os.path.isfile(args.outfile), "no checkpoint found at '{}'".format(args.outfile)
        print("loading parameters '{}'".format(args.outfile))
        checkpoint = torch.load(args.outfile, map_location=lambda storage, loc: storage)
        try:
            net.load_state_dict(checkpoint['state_dict'])
        except Exception as e:
            raise e
        print("loaded parameters '{}'".format(args.outfile))
    else:
        if torch.cuda.is_available():
            xtrain = torch.from_numpy(X[:split1].astype(np.float32)).cuda()
            ytrain = torch.from_numpy(y[:split1]).type(torch.FloatTensor).cuda()
            xvalid = torch.from_numpy(X[split1:split2].astype(np.float32)).cuda()
            yvalid = torch.from_numpy(y[split1:split2]).type(torch.FloatTensor).cuda()
            if args.approach == 'mlp_batchfair':
                ptrain = torch.from_numpy(P[:split1]).type(torch.FloatTensor).cuda()
                pvalid = torch.from_numpy(P[split1:split2]).type(torch.FloatTensor).cuda()
        else:
            xtrain = torch.from_numpy(X[:split1].astype(np.float32))
            ytrain = torch.from_numpy(y[:split1]).type(torch.FloatTensor)
            xvalid = torch.from_numpy(X[split1:split2].astype(np.float32))
            yvalid = torch.from_numpy(y[split1:split2]).type(torch.FloatTensor)
            if args.approach == 'mlp_batchfair':
                ptrain = torch.from_numpy(P[:split1]).type(torch.FloatTensor)
                pvalid = torch.from_numpy(P[split1:split2]).type(torch.FloatTensor)
        # Train
        if args.approach=='mlp_batchfair':
            appr.train(xtrain, ytrain, ptrain, xvalid, yvalid, pvalid, groups, ref)
        else:
            appr.train(xtrain,ytrain,xvalid,yvalid)

        print('-'*100)

else:
    if args.approach != 'naivebayes':
        from sklearn.model_selection import PredefinedSplit
        from sklearn.model_selection import RandomizedSearchCV
        from sklearn.externals import joblib
        xvalid = X[split1:split2]
        yvalid = y[split1:split2]

        if args.load:
            best_clf = joblib.load(args.outfile)
        else:
            print('Grid search...')
            idx_fold = [-1 for i in range(split1)]
            idx_fold.extend([0 for i in range(split2-split1)])
            clf = RandomizedSearchCV(scoring=scorers, refit= refit_score,estimator=appr, n_iter=args.niter, verbose=3,param_distributions =param_grid, n_jobs=-1,error_score=0,cv = PredefinedSplit(test_fold=idx_fold))
            try:
                clf.fit(X[:split2],y[:split2].ravel())
                print('Grid search results:', clf.cv_results_)
                print('Best score:', clf.best_score_)
                print('Best params:', clf.best_params_)
                best_clf=clf.best_estimator_
                joblib.dump(best_clf, args.outfile)
            except Exception as e:
                y[0] = 1
                best_clf = appr
                best_clf.fit(X[:split1],y[:split1].ravel())

    else:
        try:
            best_clf = appr
            best_clf.fit(X[:split1],y[:split1].ravel())
        except Exception as e:
            y[0] = 1
            best_clf = appr
            best_clf.fit(X[:split1],y[:split1].ravel())


    print('-' * 100)



############################################
# Metrics
############################################
if args.approach=='mlp' or args.approach=='mlp_batchfair':
    if torch.cuda.is_available():
        input_train = torch.from_numpy(X[:split1].astype(np.float32)).cuda()
        output_train = torch.from_numpy(y[:split1]).type(torch.FloatTensor).cuda()
        input_test = torch.from_numpy(X[split2:].astype(np.float32)).cuda()
        output_test = torch.from_numpy(y[split2:]).type(torch.FloatTensor).cuda()
        input_valid = torch.from_numpy(X[split1:split2].astype(np.float32)).cuda()
        output_valid = torch.from_numpy(y[split1:split2]).type(torch.FloatTensor).cuda()
    else:
        input_train = torch.from_numpy(X[:split1].astype(np.float32))
        output_train = torch.from_numpy(y[:split1]).type(torch.FloatTensor)
        input_test = torch.from_numpy(X[split2:].astype(np.float32))
        output_test = torch.from_numpy(y[split2:]).type(torch.FloatTensor)
        input_valid = torch.from_numpy(X[split1:split2].astype(np.float32))
        output_valid = torch.from_numpy(y[split1:split2]).type(torch.FloatTensor)
    train_scores = appr.predict(input_train)
    test_scores = appr.predict(input_test)
    valid_scores = appr.predict(input_valid)
    if torch.cuda.is_available():
        if args.mitigate==2:
            predict_fn = lambda x: appr.predict_proba(torch.from_numpy(utils.transform_interpret(mapper1.transform(pd.DataFrame(data=x,columns=data_all.columns.values)),learned_model,protected_mapped,reference_value).astype(np.float32)).cuda())
        else:
            predict_fn = lambda x: appr.predict_proba(torch.from_numpy(mapper1.transform(pd.DataFrame(data=x,columns=data_all.columns.values)).astype(np.float32)).cuda())
    else:
        if args.mitigate==2:
            predict_fn = lambda x: appr.predict_proba(torch.from_numpy(utils.transform_interpret(mapper1.transform(pd.DataFrame(data=x,columns=data_all.columns.values)),learned_model,protected_mapped,reference_value).astype(np.float32)))
        else:
            predict_fn = lambda x: appr.predict_proba(torch.from_numpy(mapper1.transform(pd.DataFrame(data=x,columns=data_all.columns.values)).astype(np.float32)))
else:
    train_scores = utils.predict_sklearn(best_clf, X[:split1])
    test_scores = utils.predict_sklearn(best_clf, X[split2:])
    valid_scores = utils.predict_sklearn(best_clf, X[split1:split2])
    if args.mitigate==2:
         predict_fn = lambda x: best_clf.predict_proba(utils.transform_interpret(mapper1.transform(pd.DataFrame(data=x,columns=data_all.columns.values)),learned_model,protected_mapped,reference_value))
    else:
        predict_fn = lambda x: best_clf.predict_proba(mapper1.transform(pd.DataFrame(data=x,columns=data_all.columns.values)))


try:
    thresholds = np.linspace(1, 0, 101)
    fpr_valid, tpr_valid, auc_thresholds_valid = roc_curve(y[split1:split2], valid_scores)
    fpr_valid = fpr_valid[auc_thresholds_valid<=1]
    tpr_valid = tpr_valid[auc_thresholds_valid<=1]
    auc_thresholds_test = auc_thresholds_valid[auc_thresholds_valid<=1]
    thresh_arr_best_ind = np.where((tpr_valid+1-fpr_valid) == np.max(tpr_valid+1-fpr_valid))[0][0]
    thresh_arr_best = np.array(auc_thresholds_test)[thresh_arr_best_ind]
except Exception as e:
    thresh_arr_best = 0.5

# fig, ax1 = plt.subplots()
# ax2 = ax1.twinx()
# ax1.hist(test_scores, bins='auto', alpha=0.2, label=args.approach)
# ax2.plot(auc_thresholds_test,0.5*(tpr_valid+1-fpr_valid), label=args.approach)
# ax2.axvline(np.array(auc_thresholds_test)[thresh_arr_best_ind], color='k', linestyle=':')
# #ax2.plot(auc_thresholds_test, fpr_test, label=args.approach)
# ax1.set_title("Histogram of predictions")
# ax1.legend(loc='best')
# plt.show()


train_predictions = train_scores > thresh_arr_best
test_predictions = test_scores > thresh_arr_best
valid_predictions = valid_scores > thresh_arr_best

buffer = ("Results for '{}' dataset, using '{}', with '{}' encoding \n".format(args.experiment,args.approach,args.encoding))

acc_train = accuracy_score(y[:split1], train_predictions)
acc_test = accuracy_score(y[split2:], test_predictions)
print("Accuracy on train: {:.4%}".format(acc_train))
print("Accuracy on test: {:.4%}".format(acc_test))
buffer += "Accuracy on train: {:.4%} \n".format(acc_train)
buffer += "Accuracy on train: {:.4%} \n".format(acc_test)

# confusion matrix on the test data.
import pandas as pd
print('\nConfusion matrix optimized for {} on the train data:'.format(refit_score))
confusionm_train=confusion_matrix(y[:split1], train_predictions)
print(pd.DataFrame(confusionm_train,columns=['pred_neg', 'pred_pos'], index=['neg', 'pos']))
fpr_train, tpr_train, auc_thresholds_train = roc_curve(y[:split1], train_scores)
auc_train=auc(fpr_train, tpr_train)
print("Area under the curve for train: {:5.4f}".format(auc_train)) # AUC of ROC
print('Confusion matrix optimized for {} on the test data:'.format(refit_score))
confusionm_test=confusion_matrix(y[split2:], test_predictions)
print(pd.DataFrame(confusionm_test,columns=['pred_neg', 'pred_pos'], index=['neg', 'pos']))
fpr_test, tpr_test, auc_thresholds_test = roc_curve(y[split2:], test_scores)
auc_test=auc(fpr_test, tpr_test)
print("Area under the curve for test: {:5.4f}".format(auc_test)) # AUC of ROC
#utils.plot_roc_curve(fpr_test, tpr_test, 'recall_optimized')
buffer += "Area under the curve on train: {:5.4f} \n".format(auc_train)
buffer += "Area under the curve on test: {:5.4f} \n".format(auc_test)

p_train,r_train,_ = precision_recall_curve(y[:split1], train_scores)
avp_train = average_precision_score(y[:split1], train_scores)
print('Average precision-recall score train: {0:0.2f}'.format(avp_train))
p_test,r_test,_ = precision_recall_curve(y[split2:], test_scores)
avp_test = average_precision_score(y[split2:], test_scores)
print('Average precision-recall score test: {0:0.2f}'.format(avp_test))
#utils.plot_pr_curve(p_test,r_test, avp_test)
buffer += 'Average precision-recall score train: {0:0.2f} \n'.format(avp_train)
buffer += 'Average precision-recall score test: {0:0.2f} \n'.format(avp_test)

data['train']['x']['score'] = train_predictions.ravel().astype(np.int).tolist()
data['train']['x']['label_value'] = y[:split1].ravel().tolist()
data['test']['x']['score'] = test_predictions.ravel().astype(np.int).tolist()
data['test']['x']['label_value'] = y[split2:].ravel().tolist()
data['valid']['x']['score'] = valid_predictions.ravel().astype(np.int).tolist()
data['valid']['x']['label_value'] = y[split1:split2].ravel().tolist()



if args.interpretability:
    ####################### Interpretability
    import lime
    import lime.lime_tabular
    explainer = lime.lime_tabular.LimeTabularExplainer(data_all.values.astype(float),feature_names=data_all.columns.values,discretize_continuous=True,categorical_features=categorical_features,categorical_names=categorical_names, verbose=False)
    explain_df_scores = pd.DataFrame(columns=data_all.columns.values)
    explain_df_categories = pd.DataFrame(columns=data_all.columns.values)
    df=pd.DataFrame({})
    for i in range(split2,data_all.shape[0]):#
        exp = explainer.explain_instance(np.ravel(data_all.iloc[[i]].values), predict_fn, num_features=data_all.shape[1])
        exp_map = exp.as_map()
        exp_list = exp.as_list()
        scores = [exp_map[1][exp_map[1][j][0]][1] for j in range(len(exp_map[1]))]
        score_id = [exp_map[1][j][0] for j in range(len(exp_map[1]))]
        categ_temp = [exp_list[j][0] for j in range(len(exp_list))]
        categ=[y for x, y in sorted(zip(score_id, categ_temp))]
        explain_df_scores.loc[i-split2] = scores
        explain_df_categories.loc[i-split2] = categ

    data['test']['explain_scores'] = explain_df_scores
    data['test']['explain_categ'] = explain_df_categories


#save predicted log probabilities
data['test']['x']['score_prob'] = test_scores.astype(np.float32).tolist()
data['test']['x']['threshold'] = float(thresh_arr_best)
data['valid']['x']['score_prob'] = valid_scores.astype(np.float32).tolist()
data['valid']['x']['threshold'] = float(thresh_arr_best)

#put back the group
for s in ['train', 'test', 'valid']:
    if 'g' in data[s]:
        data[s]['x']['group'] = data[s]['g']
    if 'putback' in data[s]:
        for c in data[s]['putback'].columns:
            data[s]['x'][c] = data[s]['putback'][c]

if args.save_scores:
    data['test']['x'].to_pickle(args.output.replace('.txt', '_scores.pkl'))
    data['valid']['x'].to_pickle(args.output.replace('.txt', '_valid_scores.pkl'))
    if args.interpretability:
        data['test']['explain_scores'].to_pickle(args.output.replace('.txt', '_explains.pkl'))
        data['test']['explain_categ'].to_pickle(args.output.replace('.txt', '_explainc.pkl'))

# Saving the objects:
with open(args.output.replace('.txt','.pkl'), 'wb') as f:
    pickle.dump([acc_train,acc_test,confusionm_train, confusionm_test,
                 p_train, r_train, avp_train, p_test,r_test, avp_test,
                 fpr_train, tpr_train, auc_train, fpr_test, tpr_test, auc_test,thresh_arr_best
                 ], f)

with open(args.output, 'w') as f:
    f.write(buffer)
