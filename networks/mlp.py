import sys
import torch
import utils

class Net(torch.nn.Module):

    def __init__(self,inputsize):
        super(Net,self).__init__()
        fact = 3
        #self.act=torch.nn.PReLU(1)
        self.act = torch.nn.Tanh()
        self.drop=torch.nn.Dropout(0.2)
        self.fc1=torch.nn.Linear(inputsize,inputsize*fact)
        self.fc2=torch.nn.Linear(inputsize*fact,inputsize*(fact+1))
        self.fc3=torch.nn.Linear(inputsize*(fact+1),1)
        self.out_act = torch.nn.Sigmoid()

        return

    def forward(self,x):
        #h=x.view(x.size(0),-1)
        h=self.drop(self.act(self.fc1(x)))
        h=self.drop(self.act(self.fc2(h)))
        y=self.out_act(self.fc3(h))
        return y


# class Net(torch.nn.Module):
#
#     def __init__(self,inputsize):
#         super(Net,self).__init__()
#         fact = 3
#         #self.act=torch.nn.PReLU(1)
#         self.act = torch.nn.Tanh()
#         self.drop=torch.nn.Dropout(0.1)
#         self.fc1=torch.nn.Linear(inputsize,inputsize*fact)
#         self.fc2=torch.nn.Linear(inputsize*fact,inputsize)
#         self.fc3 = torch.nn.Linear(inputsize, 1)
#         self.out_act = torch.nn.Sigmoid()
#
#         return
#
#     def forward(self,x):
#         #h=x.view(x.size(0),-1)
#         h=self.drop(self.act(self.fc1(x)))
#         h=self.drop(self.act(self.fc2(h)))
#         y=self.out_act(self.fc3(h))
#         return y
