import sys
import torch
import utils


class Net(torch.nn.Module):
    def __init__(self, inputsize, activation='leaky_relu'):
        super(Net, self).__init__()
        hidden_size = 50*inputsize
        self.fc1 = torch.nn.Linear(inputsize, hidden_size)
        self.activation = getattr(torch.nn.functional, activation)
        if activation in ['relu', 'leaky_relu']:
            torch.nn.init.xavier_uniform(self.fc1.weight, gain=torch.nn.init.calculate_gain(activation))
        else:
            torch.nn.init.xavier_uniform(self.fc1.weight, gain=1)
        self.fc2 = torch.nn.Linear(hidden_size, 1, bias=False)  # ELM do not use bias in the output layer.

    def forward(self, x):
        #x = x.view(x.size(0), -1)
        x = self.fc1(x)
        x = self.activation(x)
        x = self.fc2(x)
        return x

    def forwardToHidden(self, x):
        #x = x.view(x.size(0), -1)
        x = self.fc1(x)
        x = self.activation(x)
        return x
