import sys,os,argparse
import numpy as np
import scipy
import pandas as pd
import sklearn
from sklearn.metrics import roc_curve, precision_recall_curve, auc, make_scorer, recall_score, accuracy_score, precision_score, confusion_matrix, f1_score, average_precision_score
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA as PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import scipy.cluster.hierarchy as sch
from scipy.spatial.distance import pdist
from scipy.stats.stats import pearsonr
from scipy.spatial import distance
from scipy.stats import wilcoxon
from scipy.stats import ttest_ind
from scipy.stats import chi2_contingency
from sklearn.feature_selection import VarianceThreshold
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
from matplotlib import patches
import utils
import collect
import seaborn as sns

sns.set()
sns.set_context("notebook", font_scale=1.4)
sns.set_palette(sns.cubehelix_palette(8, start=.5, rot=-.75))


# Arguments
parser=argparse.ArgumentParser(description='xxx')
parser.add_argument('--experiment',default='exp1_savry',type=str,required=False,choices=['german','compas','credit','exp1_demog'],help='(default=%(default)s)')
parser.add_argument('--nseeds',type=int,default=1,help='(default=%(default)d)')
parser.add_argument('--threshold',default=0.5,type=float,required=False,help='(default=%(default)f)')
parser.add_argument('--kfolds',type=int,default=10,help='(default=%(default)d)')
parser.add_argument('--allexp',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--allsamp',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--allseed',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--violent',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--explain',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--data_path',type=str,default='dat',help='(default=%(default)s)')
parser.add_argument('--storeinparent',type=int,default=1,help='(default=%(default)s)')

args=parser.parse_args()

if args.storeinparent:
    pardir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
else:
    pardir = os.path.dirname(os.path.abspath(__file__))
resdir = os.path.join(pardir,'res')
datdir = os.path.join(pardir,args.data_path)

try:
    os.mkdir(os.path.join(pardir,'figs'))
except OSError:
    print ("Creation of the directory %s failed" % os.path.join(pardir,'figs'))

#approaches=['mlp','logit','knn','linearsvm','rbfsvm','forest','naivebayes']
#approaches=['mlp','logit','forest','linearsvm']
approaches=['logit','mlp','linearsvm','knn','tree','forest','naivebayes'] #,'knn','linearsvm','tree','forest','naivebayes'
approaches=['logit','mlp']
#encoding=['onehot','binary','backward','ordinal','helmert','sum','hashing','poly','basen','target','looe']
encodings=['onehot']

if args.allseed:
    seeds=list(range(args.nseeds))
else:
    seeds = [args.nseeds-1]

# if args.experiment in ['exp1_savry', 'exp1_demogsavry','exp1_demog','exp1_demog4000', \
#                        'exp2_savry','exp1_savry_viol','exp1_demogsavry_viol', \
#                        'exp1_demog_viol','exp1_demog4000_viol','exp1_savry_viol']:
#     approaches.append('vanilla savry sum')
#     approaches.append('vanilla savry expert')
# elif args.experiment in ['compas']:
#     approaches.append('vanilla compas')

experiments = ['german','exp1_demog','compas','credit']
experiments_names = ["German credit score","Youth recidivism CAT", "COMPAS", "Credit card default"]
experiments_names_short = ["Statlog","YouthCAT", "COMPAS", "Credit"]

# experiments = ['german','exp1_demog'] #
# experiments_names = ["German credit score","Youth recidivsm CAT"] #, "Credit card default"

if not args.allexp:
    experiments = np.array([args.experiment])

if not args.allsamp:
    samples = [1]
else:
    samples = np.round(np.arange(0.1, 1.1, 0.1),1)

fun_interpret = {
    'mean': lambda x,y,consum:np.mean(x,axis=0),
    'mean_cat': lambda x,y,consum:utils.aggregate_cat(x,y),
    'sum': lambda x,y,consum:np.mean(x,axis=0)/consum,
    'std': lambda x,y,consum:np.std(x,axis=0),
    'sqrt': lambda x,y,consum:np.sum(np.sqrt(np.abs(x)),axis=0)
}

#collect all data and compute metrics in a dictionary
metrics,datatest,amatrix = collect.collect_results(experiments=experiments,approaches=approaches,samples=samples,kfolds=args.kfolds,seeds=seeds,encodings=encodings,explain=args.explain,data_path=datdir,resdir=resdir,fun_interpret=fun_interpret)

##############################################################################
################## PLOTS
##############################################################################

if args.explain:
    if args.allexp:
        gcmatrix = np.zeros((len(experiments),len(seeds),int(len(approaches)*(len(approaches)-1)/2)))
        for fun in ['sqrt']:
            for measure in ['total']:  # ['total','tp','fp','fn','tn']
                for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
                    for s in range(len(seeds)):
                        ik=0
                        for ia in range(len(approaches)-1):
                            for ja in range(ia+1,len(approaches)):
                                gcmatrix [j,s,ik]= np.corrcoef(metrics[1][experiment][args.kfolds][encodings[0]][approaches[ia]][s]['feature_importance'][fun][measure],
                                    metrics[1][experiment][args.kfolds][encodings[0]][approaches[ja]][s]['feature_importance'][fun][measure])[0,1]
                                ik += 1
                    #import pdb;pdb.set_trace()

if args.explain:
    #################GROUP FAIRNESS - bar plots
    if args.allexp:
        measures = ['TOTAL','FP']
        measures_id = ['total_p','fp_p']
        approaches_bar = [a for a in approaches] #.replace('vanilla savry sum', 'SAVRY sum').replace('vanilla savry expert', 'Expert')
        ############## error rate balance - all experiments
        for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
            f, axarr = plt.subplots(len(measures), sharex=True,figsize=(18, 12))
            for id, measure, idm in zip(measures_id, measures, list(range(len(measures)))):
                for idpf,pf in enumerate(metrics[1][experiment][args.kfolds][encodings[0]][approaches[0]][seed]['fair']):
                    #import pdb;pdb.set_trace()
                    groups = list(metrics[1][experiment][args.kfolds][encodings[0]][approaches[0]][seed]['fair'][pf].keys())
                    groups = [g for g in groups if not metrics[sample][experiment][k][encodings[0]][approaches[0]][seed]['fair'][pf][g]['reference']]
                    barWidth = 1./len(approaches)-0.05
                    shift_exp = idpf*len(groups)
                    for i, approach in enumerate(approaches):
                        if i==0:
                            ind = np.arange(len(groups))
                        else:
                            ind_prev = ind
                            ind = np.array([xind + barWidth for xind in ind_prev])
                        if args.allseed:
                            means=[np.mean([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['feature_importance']['sqrt'][id] for k in
                                metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()]) for g in groups]
                            errors = np.transpose(np.array([np.nan_to_num(list(scipy.stats.t.interval(0.95, len(metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()) - 1, loc=np.mean([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][id] for k in
                                metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()]), scale=scipy.stats.sem([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['feature_importance']['sqrt'][id] for k in
                                metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])))) for g in groups]))
                            #import pdb;pdb.set_trace()
                        else:
                            means = [metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['feature_importance']['sqrt'][id] for g in groups]

                        if args.allseed and 'vanilla' not in approach:
                            axarr[idm].bar(shift_exp + ind, means, yerr=np.abs(errors-means),width=barWidth, edgecolor='white', label=approaches_bar[i])
                        else:
                            axarr[idm].bar(shift_exp + ind, means, width=barWidth, edgecolor='white', label=approaches_bar[i])


                    new_value = (float(idm) / len(measures)) * 0.7 + 0.22
                    if idm==0:
                        axarr[idm].text(new_value, 0.9, pf, fontsize=18, transform=plt.gcf().transFigure)
                    axarr[idm].set_prop_cycle(None)
                plt.xticks([idpf*len(groups) + g + 0.25 for g in range(len(groups)) for idpf in range(len(metrics[1][experiment][args.kfolds][encodings[0]][approaches[0]][seed]['fair']))], [g for g in (groups) for j in range(len(experiments))])
                #ax1 = plt.gca()
                axarr[idm].axhline(y=0.8,linewidth=2, color='r')
                axarr[idm].axhline(y=1.2,linewidth=2, color='r')
                #axarr[id].ylabel(measure)
                axarr[idm].set_ylabel(measure)

                if idm==0:
                    #plt.legend()
                    plt.legend(bbox_to_anchor=(0,-0.39,1,0), loc="lower left",mode="expand", borderaxespad=0, ncol=len(approaches))
                    #print(j)
                # else:
                #     #ax1 = plt.gca()
                #     axarr[idm].axvline(x=shift_exp-barWidth*0.5, linewidth=2, color='b')
        #plt.savefig(os.path.join(pardir,'figs',pf+'.pdf'), bbox_inches='tight', pad_inches=0)
            plt.show()


if args.explain:
    ntop=10
    if args.allseed:
        import functools
    ####explainability
    if args.allexp:
        for fun in ['mean_cat','sqrt']:
            for measure in ['total']:  # ['total','tp','fp','fn','tn']
                for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
                    for ia,approach in enumerate(approaches):
                        margin_bottom = None
                        if args.allseed:
                            xs = [metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['feature_importance'][fun][measure] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()]
                            sumxs=functools.reduce(lambda a,b : a+b,xs)/len(xs)
                            df_ = pd.DataFrame(index=sumxs.index,columns=['mean','std','erra','errb'])
                            df_['mean'] = np.nan_to_num(sumxs)
                            df_['std'] = np.nan_to_num(np.std(np.array(xs), axis=0))
                            df_=df_.reindex(sorted(df_.index))
                            sumxs=sumxs.reindex(sorted(sumxs.index))
                            xarr =np.zeros((len(xs),len(sumxs)))
                            for i in range(len(xs)):
                                for col in sumxs.index:
                                    if col not in xs[i]:
                                        xs[i][col]=0
                                xarr[i,:]=np.asarray(xs[i].reindex(sorted(xs[i].index)).values)
                            errors = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=sumxs.values, scale=scipy.stats.sem(xarr))))
                            df_['erra'] = errors[0]
                            df_['errb'] = errors[1]
                            abstopcols = np.abs(df_['mean']-df_['std']).sort_values()[-ntop:]
                            #import pdb;pdb.set_trace()
                            topcols = df_.loc[abstopcols.index]
                            errs=np.stack([topcols['erra'].values.ravel(),topcols['errb'].values.ravel()])
                            plt.barh(list(range(ntop)),topcols['mean'].values,xerr=np.abs(errs-topcols['mean'].values))
                            plt.yticks(list(range(ntop)),abstopcols.index)
                            print('Feature explainability for '+fun+" "+exp_name+" "+approach)
                            print(topcols[['mean','std']])
                        else:
                            abstopcols = np.abs(metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['feature_importance'][fun][measure]).sort_values()[-ntop:]
                            topcols = metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['feature_importance'][fun][measure][abstopcols.index]
                            plt.barh(list(range(ntop)),topcols.values)
                            plt.yticks(list(range(ntop)),topcols.columns)
                        plt.title('Feature explainability for '+fun+" "+exp_name+" "+approach)
                        plt.xlabel("Features")
                        plt.ylabel("Explainability")
                        plt.gca().invert_yaxis()
                        plt.show()

    if args.allexp:
        ############## GROUP FAIRNESS - error rate balance
        for measure in ['total']:  # ['total','tp','fp','fn','tn']
            for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seed]['fair']:
                groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf].keys()
                for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
                    for ia,approach in enumerate(approaches):
                        margin_bottom = None
                        # bestids=set()
                        # for i, g in enumerate(groups):
                        #     if not metrics[1][args.experiment][args.kfolds][encoding][approach][seed]['fair'][pf][g]['reference']:
                        #         best = np.argpartition(metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['disparity_feature_importance_dif']['mean_cat'][measure], -5)[-5:]
                        #         bestids = bestids.union(set(best))
                        # bestvalues=metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['disparity_feature_importance_dif']['mean_cat'][measure][list(bestids)]
                        # bestall = np.argpartition(bestvalues, -5)[-5:]
                        # bestids = np.array(list(bestids))[bestall]
                        bestticks = ['' for t in range((len(groups))*ntop)]
                        x = []
                        bestcols = []
                        for i, g in enumerate(groups):
                            bestids = np.argpartition(np.abs(metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['feature_importance']['mean_cat'][measure]), -ntop)[-ntop:]
                            if args.allseed:
                                xs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['feature_importance'][measure][bestids] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                                x = np.mean(xs)
                                intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=x, scale=scipy.stats.sem(xs))))
                            else:
                                x.append(np.take(np.array(metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['feature_importance']['mean_cat'][measure].values),bestids))
                                bestcols.append(np.take(metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['feature_importance']['mean_cat'][measure].index.values,bestids))
                                #error = np.take(np.array(metrics[1][experiment][args.kfolds][encoding][approach][seed]['fair'][pf][g]['feature_importance']['std'][measure].values),bestids)
                            if  margin_bottom is None:
                                margin_bottom = np.zeros(len(x))
                        cols = [column for column in explain_df.columns.values for a in bestcols for b in a if column in b]
                        countcols = dict((l, cols.count(l)) for l in set(cols))
                        restids = np.cumsum(np.array(list(countcols.values())))-1
                        #import pdb;pdb.set_trace()
                        for i, g in enumerate(groups):
                            xindx = [v for e in bestcols[i] for v, s in enumerate(countcols.keys()) if s in e]
                            ind = []
                            for ixx,ix in enumerate(xindx):
                                ind.append(restids[ix])
                                bestticks[restids[ix]] = bestcols[i][ixx]
                                restids[ix]-=1


                            # ind = [j*(len(groups)+1)+i for j in range(len(x))]
                            plt.barh(ind, x[i], align='center')#,left = margin_bottom
                            #margin_bottom += x[i]
                            # cond = [j * (len(groups) + 1) + i for j in range(len(x))]
                            # bestticks[i::len(groups) + 1] = bestcols
                            # import pdb;
                            # pdb.set_trace()

                        plt.yticks(list(range((len(groups))*ntop)),bestticks)
                        #plt.yticks([j*(len(groups)+1) for j in range(len(x))], explain_df.columns.values)
                        #plt.yticks([j * (len(groups) + 1) for j in range(len(x))], metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['feature_importance'][measure][bestids].columns.values)
                        #import pdb;pdb.set_trace()
                        plt.title('Feature explainability for '+exp_name+" "+approach)
                        plt.xlabel("Features")
                        plt.ylabel("Explainability")
                        plt.legend(groups)
                        plt.gca().invert_yaxis()
                        plt.show()


################## correlation of group measures, for all K folds
if args.allexp:
    labels = ['FPR','TPR','FNR','TNR','FOR','FDR','PPV','NPV','PPREV','PPR','BA','A','AUC']
    unsquareform = lambda a: a[np.nonzero(np.triu(a))]
    r_all = []
    for s in seeds:
        xdist = []
        ydist = []
        xlabels1 = []
        for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names_short)):
            X = []
            accs=[]
            aucs=[]
            labelsa = []
            for pf in metrics[1][experiment][args.kfolds][encodings[0]][approaches[0]][s]['fair']:
                groups = metrics[1][experiment][args.kfolds][encodings[0]][approaches[0]][s]['fair'][pf].keys()
                label = pf
                Xpf=[]
                aucspf=[]
                labelsapf = []
                ticks =[]
                for i,approach in enumerate(approaches):
                    # X.extend(np.array([metrics[1][experiment][args.kfolds][encoding][approach][s]['fair'][pf][g]['disparity'][0:10] for g in groups]))
                    # accs.extend(np.array([metrics[1][experiment][args.kfolds][encoding][approach][s]['fair'][pf][g]['roc'][10:] for g in groups]))
                    X.extend(np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][g]['roc'] for g in groups]))
                    aucs.extend(np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][g]['auc'] for g in groups]))
                    labelsa.extend([approach + "," + str(g)  for g in groups]) #+ "\n" + label

                    Xpf.extend(np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][g]['roc'] for g in groups]))
                    aucspf.extend(np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][g]['auc'] for g in groups]))
                    labelsapf.extend([approach + "," + str(g)  for g in groups]) #+ "\n" + label

                Xpf=np.array(Xpf)
                aucspf=np.array(aucspf)
                Xpf = np.concatenate((Xpf, np.expand_dims(aucspf, axis=0).T), axis=1)

                distx = sch.distance.pdist(Xpf, 'euclidean') #
                linkx = sch.linkage(distx, method='centroid', optimal_ordering = True)
                disty = sch.distance.pdist(Xpf.T, 'correlation') #lambda u, v: 1-np.abs(pearsonr(u,v)[0])
                linky = sch.linkage(disty, method='centroid', optimal_ordering = True)
                xdist.append(distx)
                ydist.append(disty)
                xlabels1.append(exp_name+"\n"+pf)

                sel = VarianceThreshold(threshold=0)
                Xt=sel.fit_transform(Xpf)
                variances = sel.variances_
                variances = (variances - variances.min())/(variances.max() - variances.min()) * 0.9 + 0.1
                labels_var = [labels[l] + "\n" + str('{0:1.2e}'.format(variances[l]))  for l in range(len(labels))]

                g = sns.clustermap(Xpf,row_linkage=linkx,col_linkage=linky,xticklabels=labels_var,yticklabels=labelsapf)#
                plt.show()
                g.savefig(os.path.join(pardir,'figs_appdx',exp_name+'_cluster_'+pf+'.pdf'), bbox_inches='tight', pad_inches=0)


        dist=np.array(ydist)
        r2 = np.corrcoef(dist)
        #r_array = unsquareform(r2)
        r_all.append(r2)


    r_all = np.array(r_all)
    r_mean = r_all.mean(axis=0)
    r_std = r_all.std(axis=0)
    dataframe1 = pd.DataFrame.from_records(np.round(r_mean,2)).astype(str)
    dataframe2 = pd.DataFrame.from_records(np.round(r_std,2)).astype(str)
    fig = plt.figure()
    sns.heatmap(r_mean,yticklabels=xlabels1,annot=dataframe1+'('+dataframe2+')',fmt='s')
    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off
    plt.show()
    fig.savefig(os.path.join(pardir,'figs','correlation.pdf'), bbox_inches='tight', pad_inches=0)


    # X=np.array(X)
    # print(X.shape)
    # # accs=np.array(accs)
    # auc=np.array(auc)
    # # X = np.concatenate((X, accs), axis=1)
    # X = np.concatenate((X, np.expand_dims(aucs, axis=0).T), axis=1)
    # # corr = np.zeros((X.shape[1],X.shape[1]))
    # # pcorr = np.zeros((X.shape[1],X.shape[1]))
    # #import pdb;pdb.set_trace()
    # distx = sch.distance.pdist(X, metric='euclidean')
    # print(distx.shape)
    # linkx = sch.linkage(distx, method='average')
    # disty = sch.distance.pdist(X.T, metric='euclidean')
    # print(disty.shape)
    # linky = sch.linkage(disty, method='average')
    # g = sns.clustermap(X,row_linkage=linkx,col_linkage=linky,xticklabels=labels,yticklabels=labelsa)#
    # plt.show()

    # corr_matrix = pd.DataFrame(columns=labels)
    # for i in range(X.shape[1]):
    #     corr_matrix.loc[i] = np.zeros(X.shape[1])
    #     for j in range(X.shape[1]):
    #         c1,c2=pearsonr(X[:,i], X[:,j])
    #         corr_matrix.ix[i,j] = c1
    #         #corr_matrix.loc[i].loc[j] = c1
    #         # corr[i,j] = c1
    #         # pcorr[i, j] = c1
    # plt.xticks(np.arange(13),labels)
    # plt.yticks(np.arange(13), labels)
    # plt.imshow(corr_matrix.values)
    # plt.colorbar()
    # plt.title('Correlation between different measures, experiment '+experiment)
    # plt.show()
    # d = sch.distance.pdist(corr_matrix.values)
    # L = sch.linkage(d, method='complete')
    # ind = sch.fcluster(L, 0.5*d.max(), 'distance')
    # columns = [corr_matrix.columns.tolist()[i] for i in list((np.argsort(ind)))]
    # den = sch.dendrogram(L,labels=corr_matrix.columns.tolist())
    # plt.show()
    # import pdb;pdb.set_trace()
    # corr_matrix1 = corr_matrix.reindex_axis(columns, axis=1)
    # plt.xticks(np.arange(13),corr_matrix1.columns.tolist())
    # plt.yticks(np.arange(13), corr_matrix1.columns.tolist())
    # plt.imshow(corr_matrix1.values)
    # plt.colorbar()
    # plt.title('Correlation between different measures, experiment '+experiment)
    # plt.show()
    # #import pdb;pdb.set_trace()




############## PCE visualization with axes alignment
if args.allexp:
    X_var_a = []
    for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names_short)):
        for pf in metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair']:
            X_all = []
            X_var_all = []
            for s in seeds:
                X_var = []
                X_pca = []
                X_lda = []
                X_c = []
                X = []
                y=[]
                variance_ratio = []
                groups = metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf].keys()
                #PCA decomposition is done for each of the approach separately
                for i,approach in enumerate(approaches):
                    #for j, g in enumerate(groups):
                        #if not metrics[1][args.experiment][args.kfolds][encoding][approach][s]['fair'][pf][g]['reference']:
                    X.extend(np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][g]['roc'] for g in groups]))

                    #X=np.array(X)
                    #X_2d = tsne.fit_transform(np.array(X))

                    # pca = PCA(n_components=2, random_state=0)
                    # #utils.pca_code(X[-1])
                    # X_2d = pca.fit_transform(X[-1])
                    # if pca.explained_variance_ratio_[-1]<1e-2:
                    #     pca = PCA(n_components=1, random_state=0)
                    #     X_2d = pca.fit_transform(X[-1])
                    # X_c.append(pca.components_)
                    # #import pdb;pdb.set_trace()
                    # #print(pca.explained_variance_)
                    # X_pca.append(X_2d)

                    # nc=3
                    # pca = PCA(n_components=nc, random_state=0)
                    # #utils.pca_code(X[-1])
                    # X_2d = pca.fit_transform(X[-1])
                    # nc=2
                    # while nc>=1 and pca.explained_variance_ratio_[-1]<0.1:
                    #     pca = PCA(n_components=nc, random_state=0)
                    #     X_2d = pca.fit_transform(X[-1])
                    #     nc=nc-1
                    #
                    # variance_ratio.append(pca.explained_variance_ratio_)
                    # X_c.append(pca.components_)
                    # #import pdb;pdb.set_trace()
                    # print(pca.explained_variance_ratio_)
                    # X_pca.append(X_2d)


                    y.extend(np.array([g for g in groups]))
                    # X_var.append(pca.explained_variance_ratio_)

                    #import pdb;pdb.set_trace()

                # import pdb;pdb.set_trace()
                # X=np.array(X)
                # y=np.array(y)
                # lda = LinearDiscriminantAnalysis(n_components=2,solver='lsqr', shrinkage='auto')
                # X_2r = lda.fit(X.reshape(X.shape[0]*X.shape[1],X.shape[2]),y.flatten()).transform(X.reshape(X.shape[0]*X.shape[1],X.shape[2]))

                X=np.array(X)

                pca = PCA(n_components=2, random_state=0)
                #utils.pca_code(X[-1])
                X_2d = pca.fit_transform(X)
                # if pca.explained_variance_ratio_[-1]<1e-2:
                #     pca = PCA(n_components=1, random_state=0)
                #     X_2d = pca.fit_transform(X)
                X_c.append(pca.components_)
                #import pdb;pdb.set_trace()
                #print(pca.explained_variance_)
                X_pca=X_2d

                X_var.append(pca.explained_variance_ratio_)

                X_pca = X_pca.reshape(len(approaches),len(groups),-1)
                X_c = np.array(X_c)
                X_lda = np.array(X_pca)
                #X_var = np.array(X_var)

                # #we align the pca eigenvectors with respect to the eigenvectors of the first approach
                # projection_matrix=np.ones((len(approaches),X_pca.shape[-1],X_c.shape[-1],X_c.shape[-1]))
                # for i in range(1,len(approaches)):
                #     eigvect_aligned = []
                #     for k in range(X_pca.shape[-1]):
                #         projection_matrix=np.outer(X_c[0,k],X_c[i,k].T) #P eig1=eig2
                #         eigvect_aligned.append(np.dot(projection_matrix, X_c[i,k])) #eig1_alig=P*eig1
                #     eigvect_aligned = np.array(eigvect_aligned)
                #     # import pdb;
                #     # pdb.set_trace()
                #     X_pca[i]=np.dot(eigvect_aligned, X[i].T).T

                #import pdb;pdb.set_trace()
                ####we need to center the reference group for all the approaches
                for i, approach in enumerate(approaches):
                    for j, group in enumerate(groups):
                        if  metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][group]['reference']:
                            ref = X_pca[i,j]
                    for j, group in enumerate(groups):
                            X_pca[i,j] = X_pca[i,j] - ref

                # #after PCA the axes might have different orientation for different approaches, so we need to flip them
                # dist =  np.zeros((len(approaches),len(groups))) #we compute the distance of each group to the origin
                # for i, approach in enumerate(approaches):
                #     for j, group in enumerate(groups):
                #         # import pdb;
                #         # pdb.set_trace()
                #         dist[i,j] = np.sum(distance.euclidean(X_pca[i,j], np.zeros_like(X_pca[i,j])))
                #
                # indices_max =  np.unravel_index(np.argmax(dist), dist.shape)
                # signs = np.sign(X_pca[indices_max])
                #
                # #flip the axis of the aproach if different from the one of the maximum
                # for i, approach in enumerate(approaches):
                #     if i!=indices_max[0]:
                #         for k,s in enumerate(signs):
                #             if s!=0 and np.sign(X_pca[i,indices_max[1],k])!=s:
                #                 X_pca[i, :, k] = X_pca[i,:,k] * (-1.) #flip the axis

                #X_pca = X_pca + np.ones_like(X_pca)

                X_all.append(X_pca)
                X_var_all.append(X_var)


            X_all = np.array(X_all)
            X_var_all = np.array(X_var_all)
            X_var_a.extend(X_var_all)
            X_pca = X_all.mean(axis=0)
            var_r=X_var_all.mean(axis=(0,1))
            # fig = plt.figure()
            # for j, group in enumerate(groups):
            #     #if not metrics[1][args.experiment][args.kfolds][encoding][approach][seed]['fair'][pf][group]['reference']:
            #     ax1 = plt.gca()
            #     for i, approach in enumerate(approaches):
            #         color = next(ax1._get_lines.prop_cycler)['color']
            #         if X_var_all.shape[-1]==2:
            #             plt.scatter(X_pca[i, j, 0], X_pca[i, j, 1], color=color, marker='o', s=60, label='{}'.format(approach))
            #             plt.text(X_pca[i,j, 0] * (1 + 0.01), X_pca[i,j, 1] * (1 + 0.01), group, fontsize=8)
            #             if args.allseed:
            #                 intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(X_all[:,i, j, 0]) - 1, loc=X_pca[i, j, 0], scale=scipy.stats.sem(X_all[:,i, j, 0]))))
            #                 inty = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(X_all[:,i, j, 1]) - 1, loc=X_pca[i, j, 1], scale=scipy.stats.sem(X_all[:,i, j, 1]))))
            #                 el = patches.Ellipse((X_pca[i, j, 0], X_pca[i, j, 1]), (intx[1]-intx[0]), (inty[1]-inty[0]),fill=True, alpha=0.2,color=color)
            #                 #el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
            #                 ax1.add_patch(el)
            #                 ax1.add_artist(el)
            #         else:
            #             plt.scatter(X_pca[i, j, 0], 0, color=color, marker='o', s=60, label='{}'.format(approach))
            #             plt.text(X_pca[i, j, 0] * (1 + 0.01),0 * (1 + 0.01), group, fontsize=16)
            #             if args.allseed:
            #                 #import pdb; pdb.set_trace()
            #                 intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(X_all[:,i, j, 0]) - 1, loc=X_pca[i, j, 0], scale=scipy.stats.sem(X_all[:,i, j, 0]))))
            #                 el = patches.Ellipse((X_pca[i, j, 0], 0), (intx[1]-1e-4), (intx[1]-1e-4),fill=True, alpha=0.2,color=color)
            #                 #el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
            #                 ax1.add_patch(el)
            #                 ax1.add_artist(el)
            #
            #     if j==0:
            #         plt.legend()
            #     plt.gca().set_prop_cycle(None)
            #
            # #plt.title('PCA decomposition, ' + experiment+', variance ratio means '+str(np.round(var_r,2))) #+',PC1 '+str(np.round(pca.explained_variance_ratio_[0],2))+',PC2 '+str(np.round(pca.explained_variance_ratio_[1],2))
            # #plt.title('variance ratio means '+str(np.round(var_r,2)))
            # print('variance ratio means '+str(np.round(var_r,2)))
            # plt.show()
            # fig.savefig(os.path.join(pardir,'figs','pca_'+pf+'.pdf'), bbox_inches='tight', pad_inches=0)

    X_var_a = np.array(X_var_a)
    var_r=X_var_all.mean(axis=(0,1))
    print(var_r)


############## PCE visualization on all approaches
if args.allexp:
    for experiment in experiments:
        for pf in metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair']:
            X_all = []
            X_var_all = []
            for s in seeds:
                X_var = []
                X_pca = []
                X_lda = []
                X_c = []
                X = []
                y=[]
                variance_ratio = []
                groups = metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf].keys()
                #PCA decomposition is done for each of the approach separately
                for i,approach in enumerate(approaches):
                    #for j, g in enumerate(groups):
                        #if not metrics[1][args.experiment][args.kfolds][encoding][approach][s]['fair'][pf][g]['reference']:
                    X.append(np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][g]['roc'] for g in groups]))

                    #X=np.array(X)
                    #X_2d = tsne.fit_transform(np.array(X))
                    pca = PCA(n_components=2, random_state=0)
                    #utils.pca_code(X[-1])
                    X_2d = pca.fit_transform(X[-1])
                    # if pca.explained_variance_ratio_[-1]<1e-2:
                    #     pca = PCA(n_components=1, random_state=0)
                    #     X_2d = pca.fit_transform(X[-1])
                    X_c.append(pca.components_)
                    #import pdb;pdb.set_trace()
                    #print(pca.explained_variance_)
                    X_pca.append(X_2d)
                    # nc=3
                    # pca = PCA(n_components=nc, random_state=0)
                    # #utils.pca_code(X[-1])
                    # X_2d = pca.fit_transform(X[-1])
                    # nc=2
                    # while nc>=1 and pca.explained_variance_ratio_[-1]<0.1:
                    #     pca = PCA(n_components=nc, random_state=0)
                    #     X_2d = pca.fit_transform(X[-1])
                    #     nc=nc-1
                    #
                    # variance_ratio.append(pca.explained_variance_ratio_)
                    # X_c.append(pca.components_)
                    # #import pdb;pdb.set_trace()
                    # print(pca.explained_variance_ratio_)
                    # X_pca.append(X_2d)

                    #LDA
                    y.append(np.array([g for g in groups]))
                    X_var.append(pca.explained_variance_ratio_)
                    #import pdb;pdb.set_trace()

                # import pdb;pdb.set_trace()
                # X=np.array(X)
                # y=np.array(y)
                # lda = LinearDiscriminantAnalysis(n_components=2,solver='lsqr', shrinkage='auto')
                # X_2r = lda.fit(X.reshape(X.shape[0]*X.shape[1],X.shape[2]),y.flatten()).transform(X.reshape(X.shape[0]*X.shape[1],X.shape[2]))

                X_pca = np.array(X_pca)
                X_c = np.array(X_c)
                X_lda = np.array(X_pca)
                #X_var = np.array(X_var)

                #we align the pca eigenvectors with respect to the eigenvectors of the first approach
                projection_matrix=np.ones((len(approaches),X_pca.shape[-1],X_c.shape[-1],X_c.shape[-1]))
                for i in range(1,len(approaches)):
                    eigvect_aligned = []
                    for k in range(X_pca.shape[-1]):
                        projection_matrix=np.outer(X_c[0,k],X_c[i,k].T) #P eig1=eig2
                        #import pdb;pdb.set_trace()
                        eigvect_aligned.append(np.dot(projection_matrix, X_c[i,k])) #eig1_alig=P*eig1
                    eigvect_aligned = np.array(eigvect_aligned)
                    # import pdb;
                    # pdb.set_trace()
                    X_pca[i]=np.dot(eigvect_aligned, X[i].T).T

                ####we need to center the reference group for all the approaches
                for i, approach in enumerate(approaches):
                    for j, group in enumerate(groups):
                        if  metrics[1][experiment][args.kfolds][encodings[0]][approach][s]['fair'][pf][group]['reference']:
                            ref = X_pca[i,j]
                    for j, group in enumerate(groups):
                            X_pca[i,j] = X_pca[i,j] - ref

                #after PCA the axes might have different orientation for different approaches, so we need to flip them
                dist =  np.zeros((len(approaches),len(groups))) #we compute the distance of each group to the origin
                for i, approach in enumerate(approaches):
                    for j, group in enumerate(groups):
                        # import pdb;
                        # pdb.set_trace()
                        dist[i,j] = np.sum(distance.euclidean(X_pca[i,j], np.zeros_like(X_pca[i,j])))
                indices_max = np.argmax(dist,axis=1)
                # if pf=='national_group':
                #     import pdb;pdb.set_trace()
                #flip the axis of the aproach if different from the one of the maximum
                for j in range(len(indices_max)):
                    amax = np.argmax(dist[:,indices_max[j]])
                    for i, approach in enumerate(approaches):
                        if i!=amax and np.sign(X_pca[i,indices_max[j],j])!=np.sign(X_pca[amax,indices_max[j],j]):
                            X_pca[i,indices_max[j],j] = X_pca[i,indices_max[j],j] * (-1.) #flip the axis


                #X_pca = X_pca + np.ones_like(X_pca)

                X_all.append(X_pca)
                X_var_all.append(X_var)
                #import pdb; pdb.set_trace()

            X_all = np.array(X_all)
            #X_var_all = np.array(X_var_all)
            X_pca = X_all.mean(axis=0)
            fig = plt.figure()
            for j, group in enumerate(groups):
                #if not metrics[1][args.experiment][args.kfolds][encoding][approach][seed]['fair'][pf][group]['reference']:
                ax1 = plt.gca()
                for i, approach in enumerate(approaches):
                    color = next(ax1._get_lines.prop_cycler)['color']
                    if args.allseed:
                        var_ratio=np.array(X_var_all[:][i]).mean(axis=0)
                    else:
                        var_ratio=X_var_all[0][i]
                    if X_var_all[0][i].shape[-1]==2:
                        plt.scatter(X_pca[i, j, 0], X_pca[i, j, 1], color=color, marker='o', s=100, label='{}'.format(approach))
                        plt.text(X_pca[i,j, 0] * (1 + 0.01), X_pca[i,j, 1] * (1 + 0.01), group, fontsize=16)
                        if args.allseed:
                            intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(X_all[:,i, j, 0]) - 1, loc=X_pca[i, j, 0], scale=scipy.stats.sem(X_all[:,i, j, 0]))))
                            inty = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(X_all[:,i, j, 1]) - 1, loc=X_pca[i, j, 1], scale=scipy.stats.sem(X_all[:,i, j, 1]))))
                            el = patches.Ellipse((X_pca[i, j, 0], X_pca[i, j, 1]), (intx[1]-intx[0]), (inty[1]-inty[0]),fill=True, alpha=0.2,color=color)
                            #el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
                            ax1.add_patch(el)
                            ax1.add_artist(el)
                    else:
                        plt.scatter(X_pca[i, j, 0], 0, color=color, marker='o', s=100, label='{}'.format(approach))
                        plt.text(X_pca[i, j, 0] * (1 + 0.01),0 * (1 + 0.01), group, fontsize=16)
                        if args.allseed:
                            #import pdb; pdb.set_trace()
                            intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(X_all[:,i, j, 0]) - 1, loc=X_pca[i, j, 0], scale=scipy.stats.sem(X_all[:,i, j, 0]))))
                            el = patches.Ellipse((X_pca[i, j, 0], 0), (intx[1]-1e-4), (intx[1]-1e-4),fill=True, alpha=0.2,color=color)
                            #el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
                            ax1.add_patch(el)
                            ax1.add_artist(el)

                if j==0:
                    plt.legend()
                plt.gca().set_prop_cycle(None)

            #plt.title('PCA decomposition, ' + experiment+', variance ratio '+str(np.round(var_ratio,2))) #+',PC1 '+str(np.round(pca.explained_variance_ratio_[0],2))+',PC2 '+str(np.round(pca.explained_variance_ratio_[1],2))
            print('PCA decomposition, ' + experiment+', variance ratio '+str(np.round(var_ratio,2)))
            plt.show()
            fig.savefig(os.path.join(pardir,'figs','pca_'+pf+'.pdf'), bbox_inches='tight', pad_inches=0)







#import pdb;pdb.set_trace()
if args.allexp:
    if args.allseed:
        for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
            for approach in approaches:
                aucs1 = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['auc'] for k in
                                  metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                print('{}, {}, mean={}, std={}'.format(exp_name, approach, np.round(np.mean(aucs1), 2),
                                                       np.round(np.std(aucs1), 4)))
