import sys,os,argparse
import numpy as np
import scipy
import pandas as pd
import sklearn
from sklearn.metrics import roc_curve, precision_recall_curve, auc, make_scorer, recall_score, accuracy_score, precision_score, confusion_matrix, f1_score, average_precision_score
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA as PCA
from scipy.stats.stats import pearsonr
from scipy.spatial import distance
from scipy.stats import wilcoxon
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
from matplotlib import patches
import utils
import collect
import seaborn as sns

sns.set()
sns.set_context("notebook", font_scale=1.8)
sns.set_palette(sns.cubehelix_palette(8, start=.5, rot=-.75))

# Arguments
parser=argparse.ArgumentParser(description='xxx')
parser.add_argument('--experiment',default='exp1_savry',type=str,required=False,choices=['german','compas','adult','exp1_savry', \
                                                                              'exp1_demogsavry','exp1_demog','exp1_demog4000', \
                                                                              'exp2_savry','exp1_savry_viol','exp1_demogsavry_viol', \
                                                                              'exp1_demog_viol','exp1_demog4000_viol'],help='(default=%(default)s)')
parser.add_argument('--nseeds',type=int,default=1,help='(default=%(default)d)')
parser.add_argument('--threshold',default=0.5,type=float,required=False,help='(default=%(default)f)')
parser.add_argument('--kfolds',type=int,default=10,help='(default=%(default)d)')
parser.add_argument('--allexp',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--allsamp',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--allseed',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--violent',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--explain',type=int,default=0,help='(default=%(default)d)')
parser.add_argument('--data_path',type=str,default='dat',help='(default=%(default)s)')
parser.add_argument('--storeinparent',type=int,default=1,help='(default=%(default)s)')

args=parser.parse_args()

if args.storeinparent:
    pardir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
else:
    pardir = os.path.dirname(os.path.abspath(__file__))
resdir = os.path.join(pardir,'res')
datdir = os.path.join(pardir,args.data_path)

try:
    os.mkdir(os.path.join(pardir,'figs'))
except OSError:
    print ("Creation of the directory %s failed" % os.path.join(pardir,'figs'))

#approaches=['mlp','logit','knn','linearsvm','rbfsvm','forest','naivebayes']
#approaches=['mlp','logit','forest','linearsvm']
approaches=['logit','mlp']
#encoding=['onehot','binary','backward','ordinal','helmert','sum','hashing','poly','basen','target','looe']
encodings=['onehot']

if args.allseed:
    seeds=list(range(args.nseeds))
else:
    seeds = [args.nseeds-1]

if args.experiment in ['exp1_savry', 'exp1_demogsavry','exp1_demog','exp1_demog4000', \
                       'exp2_savry','exp1_savry_viol','exp1_demogsavry_viol', \
                       'exp1_demog_viol','exp1_demog4000_viol','exp1_savry_viol']:
    approaches.append('vanilla savry sum')
    approaches.append('vanilla savry expert')
elif args.experiment in ['compas']:
    approaches.append('vanilla compas')

if "savry" in args.experiment or "demog" in args.experiment:
    if args.experiment == 'exp2_savry':
        experiments = ['exp2_savry']
    else:
        experiments = ['exp1_savry', 'exp1_demog','exp1_demogsavry'] #, 'exp1_demog4000'
        experiments_names = ["SAVRY ML", "Static ML", "Static+SAVRY ML"] # , "demographics more data"

elif "compas" in args.experiment:
    experiments = ["compas"]

if args.violent:
    experiments = [e+"_viol" for e in experiments]
    experiments_names = [e + " violent" for e in experiments_names]

if not args.allexp:
    experiments = np.array([args.experiment])

if not args.allsamp:
    samples = [1]
else:
    samples = np.round(np.arange(0.1, 1.1, 0.1),1)

fun_interpret = {
    'mean': lambda x,y,consum:np.mean(x,axis=0),
    'mean_cat': lambda x,y,consum:utils.aggregate_cat(x,y),
    'sum': lambda x,y,consum:np.mean(x,axis=0)/consum,
    'std': lambda x,y,consum:np.std(x,axis=0),
    'sqrt': lambda x,y,consum:np.sum(np.sqrt(np.abs(x)),axis=0)
}


df=None
buffer=""

# p_auc_all=[]
# ROC_all=[]
# ROC_disparity_all=[]
# ROC1_all=[]
# auc_all=[]
# acc_all=[]
# fpr_all=[]
# tpr_all=[]
# p_all=[]
# r_all=[]
# avp_all=[]
metrics = {}

#collect all data and compute metrics in a dictionary
metrics,datatest,amatrix = collect.collect_results(experiments=experiments,approaches=approaches,samples=samples,kfolds=args.kfolds,seeds=seeds,encodings=encodings,explain=args.explain,data_path=datdir,resdir=resdir,fun_interpret=fun_interpret)


##############################################################################
################## PLOTS
##############################################################################

if args.explain:
    ntop=10
    if args.allseed:
        import functools
    ####explainability
    if args.allexp:
        for fun in ['mean_cat','sqrt']:
            for measure in ['total']:  # ['total','tp','fp','fn','tn']
                for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
                    for ia,approach in enumerate(approaches):
                        if 'vanilla' not in approach:
                            margin_bottom = None
                            if args.allseed:
                                xs = [metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['feature_importance'][fun][measure] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()]
                                sumxs=functools.reduce(lambda a,b : a+b,xs)/len(xs)
                                df_ = pd.DataFrame(index=sumxs.index,columns=['mean','std','erra','errb'])
                                df_['mean'] = np.nan_to_num(sumxs)
                                df_['std'] = np.nan_to_num(np.std(np.array(xs), axis=0))
                                df_=df_.reindex(sorted(df_.index))
                                sumxs=sumxs.reindex(sorted(sumxs.index))
                                xarr =np.zeros((len(xs),len(sumxs)))
                                for i in range(len(xs)):
                                    for col in sumxs.index:
                                        if col not in xs[i]:
                                            xs[i][col]=0
                                    xarr[i,:]=np.asarray(xs[i].reindex(sorted(xs[i].index)).values)
                                errors = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=sumxs.values, scale=scipy.stats.sem(xarr))))
                                df_['erra'] = errors[0]
                                df_['errb'] = errors[1]
                                abstopcols = np.abs(df_['mean']-df_['std']).sort_values()[-ntop:]
                                #import pdb;pdb.set_trace()
                                topcols = df_.loc[abstopcols.index]
                                errs=np.stack([topcols['erra'].values.ravel(),topcols['errb'].values.ravel()])
                                plt.barh(list(range(ntop)),topcols['mean'].values,xerr=np.abs(errs-topcols['mean'].values))
                                plt.yticks(list(range(ntop)),abstopcols.index)
                                print('Feature explainability for '+fun+" "+exp_name+" "+approach)
                                print(topcols[['mean','std']])
                            else:
                                abstopcols = np.abs(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['feature_importance'][fun][measure]).sort_values()[-ntop:]
                                topcols = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['feature_importance'][fun][measure][abstopcols.index]
                                plt.barh(list(range(ntop)),topcols.values)
                                plt.yticks(list(range(ntop)),topcols.columns)
                            plt.title('Feature explainability for '+fun+" "+exp_name+" "+approach)
                            plt.xlabel("Features")
                            plt.ylabel("Explainability")
                            plt.gca().invert_yaxis()
                            plt.show()

    if args.allexp:
        ############## GROUP FAIRNESS - error rate balance
        for measure in ['total']:  # ['total','tp','fp','fn','tn']
            for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
                groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
                for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
                    for ia,approach in enumerate(approaches):
                        if 'vanilla' not in approach:
                            margin_bottom = None
                            # bestids=set()
                            # for i, g in enumerate(groups):
                            #     if not metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
                            #         best = np.argpartition(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity_feature_importance_dif']['mean_cat'][measure], -5)[-5:]
                            #         bestids = bestids.union(set(best))
                            # bestvalues=metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity_feature_importance_dif']['mean_cat'][measure][list(bestids)]
                            # bestall = np.argpartition(bestvalues, -5)[-5:]
                            # bestids = np.array(list(bestids))[bestall]
                            bestticks = ['' for t in range((len(groups))*ntop)]
                            x = []
                            bestcols = []
                            for i, g in enumerate(groups):
                                bestids = np.argpartition(np.abs(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['feature_importance']['mean_cat'][measure]), -ntop)[-ntop:]
                                if args.allseed:
                                    xs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['feature_importance'][measure][bestids] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                                    x = np.mean(xs)
                                    intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=x, scale=scipy.stats.sem(xs))))
                                else:
                                    x.append(np.take(np.array(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['feature_importance']['mean_cat'][measure].values),bestids))
                                    bestcols.append(np.take(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['feature_importance']['mean_cat'][measure].index.values,bestids))
                                    #error = np.take(np.array(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['feature_importance']['std'][measure].values),bestids)
                                if  margin_bottom is None:
                                    margin_bottom = np.zeros(len(x))
                            cols = [column for column in explain_df.columns.values for a in bestcols for b in a if column in b]
                            countcols = dict((l, cols.count(l)) for l in set(cols))
                            restids = np.cumsum(np.array(list(countcols.values())))-1
                            #import pdb;pdb.set_trace()
                            for i, g in enumerate(groups):
                                xindx = [v for e in bestcols[i] for v, s in enumerate(countcols.keys()) if s in e]
                                ind = []
                                for ixx,ix in enumerate(xindx):
                                    ind.append(restids[ix])
                                    bestticks[restids[ix]] = bestcols[i][ixx]
                                    restids[ix]-=1


                                # ind = [j*(len(groups)+1)+i for j in range(len(x))]
                                plt.barh(ind, x[i], align='center')#,left = margin_bottom
                                #margin_bottom += x[i]
                                # cond = [j * (len(groups) + 1) + i for j in range(len(x))]
                                # bestticks[i::len(groups) + 1] = bestcols
                                # import pdb;
                                # pdb.set_trace()

                            plt.yticks(list(range((len(groups))*ntop)),bestticks)
                            #plt.yticks([j*(len(groups)+1) for j in range(len(x))], explain_df.columns.values)
                            #plt.yticks([j * (len(groups) + 1) for j in range(len(x))], metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['feature_importance'][measure][bestids].columns.values)
                            #import pdb;pdb.set_trace()
                            plt.title('Feature explainability for '+exp_name+" "+approach)
                            plt.xlabel("Features")
                            plt.ylabel("Explainability")
                            plt.legend(groups)
                            plt.gca().invert_yaxis()
                            plt.show()



# if not args.allexp:
#     ############## GROUP FAIRNESS - error rate balance
#     for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
#         groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
#         aucs = np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'] for g in groups])
#         ax1 = plt.gca()
#         for i,g in enumerate(groups):
#             if not metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
#                 for approach in approaches:
#                     if 'vanilla' not in approach:
#                         if args.allseed:
#                             xs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fp'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
#                             ys = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fn'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
#                             x = np.mean(xs)
#                             y = np.mean(ys)
#                             intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=x, scale=scipy.stats.sem(xs))))
#                             inty = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(ys) - 1, loc=x, scale=scipy.stats.sem(ys))))
#                         else:
#                             x = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fp']
#                             y = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fn']
#
#                         if args.allseed:
#                             color = next(ax1._get_lines.prop_cycler)['color']
#                             if 'vanilla' not in approach:
#                                 el = patches.Ellipse((x, y), (intx[1]-intx[0]), (inty[1]-inty[0]),fill=True, alpha=0.2,color=color)
#                                 #el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
#                                 ax1.add_patch(el)
#                                 ax1.add_artist(el)
#                             plt.scatter(x, y, marker='o', color=color, label='{}, {}'.format(approach, g),s=5)
#                         else:
#                             size =  (aucs[i] - aucs.min()) / (aucs.max() - aucs.min())
#                             plt.scatter(x,y, marker='o',  label='{}, {}'.format(approach,g),s=5)#s=200*(size+0.1))
#                         plt.text(x * (1 + 0.01), y * (1 + 0.01), g, fontsize=8)
#                 plt.gca().set_prop_cycle(None)
#
#         plt.title('Feature interpretability group difference')
#         plt.xlabel("FP group absolute difference")
#         plt.ylabel("FN group absolute difference")
#         plt.legend()
#         plt.show()
#
# else:
#     ############## GROUP FAIRNESS - error rate balance - all experiments
#     markers=['o','s','^','P','D','X','H']
#     for pf in metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
#         ax1 = plt.gca()
#         for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
#             groups = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
#             aucs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'] for g in groups])
#             for i,g in enumerate(groups):
#                 if not metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
#                     for approach in approaches:
#                         if 'vanilla' not in approach:
#                             label = '{} on {}, {}'.format(approach, exp_name, g)
#                             if args.allseed:
#                                 xs = np.array([metrics[1][experiment][args.kfolds][experiment][approach][k]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fp'] for k in
#                                                metrics[1][experiment][args.kfolds][experiment][approach].keys()])
#                                 ys = np.array([metrics[1][experiment][args.kfolds][experiment][approach][k]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fn'] for k in
#                                                metrics[1][experiment][args.kfolds][experiment][approach].keys()])
#                                 x = np.mean(xs)
#                                 y = np.mean(ys)
#                                 intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=x, scale=scipy.stats.sem(xs))))
#                                 inty = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(ys) - 1, loc=x, scale=scipy.stats.sem(ys))))
#                             else:
#                                 x = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fp']
#                                 y = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity_feature_importance_sum']['mean']['fn']
#
#                             if args.allseed:
#                                 color = next(ax1._get_lines.prop_cycler)['color']
#
#                                 if j == 0:
#                                     plt.scatter(x, y,color=color, marker='o', label='{}, {}'.format(approach, g), s=5)
#                             else:
#                                 size =  (aucs[i] - aucs.min()) / (aucs.max() - aucs.min())
#                                 if j == 0 or 'vanilla' not in approach:
#                                     plt.scatter(x,y, marker=markers[j], label=label,s=5)#s=200*(size+0.1))
#                             plt.text(x * (1 + 0.01), y * (1 + 0.01), g, fontsize=8)
#
#                     plt.gca().set_prop_cycle(None)
#
#         plt.title('Feature interpretability group difference')
#         plt.xlabel("FP group absolute difference")
#         plt.ylabel("FN group absolute difference")
#         plt.legend()
#         ax1 = plt.gca()
#         ax1.add_patch(matplotlib.patches.Rectangle((0.8, 0.8), 0.4, 0.4,alpha=0.1, facecolor='green'))
#         plt.show()


# ############## PCE OR TSNE visualization
#
# # for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
# #     groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
# #     X = []
# #     for j, g in enumerate(groups):
# #         for i,approach in enumerate(approaches):
# #             X.extend(np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'] for g in groups]))
# #     pca = PCA(n_components=2, random_state=0)
# #     X_2d = pca.fit_transform(np.array(X))
# #     import pdb;pdb.set_trace()
# #     id=0
# #     for j, group in enumerate(groups):
# #         for i, approach in enumerate(approaches):
# #             plt.scatter(X_2d[id, 0], X_2d[id, 1], marker='o',s=60, label='{}'.format(approach))
# #             plt.text(X_2d[id, 0] * (1 + 0.01), X_2d[id, 1] * (1 + 0.01), group, fontsize=8)
# #             #import pdb;pdb.set_trace()
# #             id=id+1
# #         if j==0:
# #             plt.legend()
# #         plt.gca().set_prop_cycle(None)
# #     plt.legend()
# #     plt.title('PCA decomposition, ' + args.experiment)  # +',PC1 '+str(np.round(pca.explained_variance_ratio_[0],2))+',PC2 '+str(np.round(pca.explained_variance_ratio_[1],2))
# #     plt.show()
#
#
# for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
#     X_pca = []
#     X_c = []
#     X = []
#     variance_ratio = []
#     groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
#     #PCA decomposition is done for each of the approach separately
#     for i,approach in enumerate(approaches):
#         #for j, g in enumerate(groups):
#             #if not metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
#         X.append(np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['roc'] for g in groups]))
#         #X=np.array(X)
#         #X_2d = tsne.fit_transform(np.array(X))
#         pca = PCA(n_components=2, random_state=0)
#         #utils.pca_code(X[-1])
#         X_2d = pca.fit_transform(X[-1])
#         variance_ratio.append(pca.explained_variance_ratio_)
#         if pca.explained_variance_ratio_[-1]<1e-2:
#             pca = PCA(n_components=1, random_state=0)
#             X_2d = pca.fit_transform(X[-1])
#         X_c.append(pca.components_)
#         #import pdb;pdb.set_trace()
#         #print(pca.explained_variance_)
#         X_pca.append(X_2d)
#
#     X_pca = np.array(X_pca)
#     X_c = np.array(X_c)
#
#     #we align the pca eigenvectors with respect to the eigenvectors of the first approach
#     projection_matrix=np.ones((len(approaches),X_pca.shape[-1],X_c.shape[-1],X_c.shape[-1]))
#     for i in range(1,len(approaches)):
#         eigvect_aligned = []
#         for k in range(X_pca.shape[-1]):
#             projection_matrix=np.outer(X_c[0,k],X_c[i,k].T) #P eig1=eig2
#             eigvect_aligned.append(np.dot(projection_matrix, X_c[i,k])) #eig1_alig=P*eig1
#         eigvect_aligned = np.array(eigvect_aligned)
#         # import pdb;
#         # pdb.set_trace()
#         X_pca[i]=np.dot(eigvect_aligned, X[i].T).T
#
#     ####we need to center the reference group for all the approaches
#     for i, approach in enumerate(approaches):
#         for j, group in enumerate(groups):
#             if  metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][group]['reference']:
#                 ref = X_pca[i,j]
#         for j, group in enumerate(groups):
#                 X_pca[i,j] = X_pca[i,j] - ref
#
#     # #after PCA the axes might have different orientation for different approaches, so we need to flip them
#     # dist =  np.zeros((len(approaches),len(groups))) #we compute the distance of each group to the origin
#     # for i, approach in enumerate(approaches):
#     #     for j, group in enumerate(groups):
#     #         # import pdb;
#     #         # pdb.set_trace()
#     #         dist[i,j] = np.sum(distance.euclidean(X_pca[i,j], np.zeros_like(X_pca[i,j])))
#     #
#     # indices_max =  np.unravel_index(np.argmax(dist), dist.shape)
#     # signs = np.sign(X_pca[indices_max])
#     #
#     # #flip the axis of the aproach if different from the one of the maximum
#     # for i, approach in enumerate(approaches):
#     #     if i!=indices_max[0]:
#     #         for k,s in enumerate(signs):
#     #             if s!=0 and np.sign(X_pca[i,indices_max[1],k])!=s:
#     #                 X_pca[i, :, k] = X_pca[i,:,k] * (-1.) #flip the axis
#
#     #X_pca = X_pca + np.ones_like(X_pca)
#
#     for j, group in enumerate(groups):
#         #if not metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][group]['reference']:
#         for i, approach in enumerate(approaches):
#             #plt.scatter(X_pca[i,j, 0], X_pca[i,j, 1], marker='o',s=60, label='{},{}, AUC={}'.format(approach,group,np.round(metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][group]['auc'], 2)))
#             if len(pca.explained_variance_ratio_)==2:
#                 plt.scatter(X_pca[i, j, 0], X_pca[i, j, 1], marker='o', s=60, label='{}'.format(approach))
#                 plt.text(X_pca[i,j, 0] * (1 + 0.01), X_pca[i,j, 1] * (1 + 0.01), group, fontsize=8)
#                 var_ratio = np.array(variance_ratio).mean(axis=0)
#             else:
#                 plt.scatter(X_pca[i, j, 0], 0, marker='o', s=60, label='{}'.format(approach))
#                 plt.text(X_pca[i, j, 0] * (1 + 0.01),0 * (1 + 0.01), group, fontsize=8)
#                 var_ratio = np.array(variance_ratio).mean(axis=0)
#         if j==0:
#             plt.legend()
#         plt.gca().set_prop_cycle(None)
#
#
#     plt.title('PCA decomposition, ' + args.experiment+' ,variance ratio '+str(np.round(var_ratio,2))) #+',PC1 '+str(np.round(pca.explained_variance_ratio_[0],2))+',PC2 '+str(np.round(pca.explained_variance_ratio_[1],2))
#     plt.show()
#
#



################### correlation of group measures, for all K folds
# X = []
# accs=[]
# aucs=[]
# #for k in range(2,14):
# for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
#     groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
#     for j, g in enumerate(groups):
#         #if not metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
#         for i,approach in enumerate(approaches):
#             X.extend(np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'][0:10] for g in groups]))
#             accs.extend(np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['roc'][10:] for g in groups]))
#             aucs.extend(np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'] for g in groups]))
# X=np.array(X)
# accs=np.array(accs)
# auc=np.array(auc)
# X = np.concatenate((X, accs), axis=1)
# X = np.concatenate((X, np.expand_dims(aucs, axis=0).T), axis=1)
# corr = np.zeros((X.shape[1],X.shape[1]))
# pcorr = np.zeros((X.shape[1],X.shape[1]))
# for i in range(X.shape[1]):
#     for j in range(X.shape[1]):
#         c1,c2=pearsonr(X[:,i], X[:,j])
#         corr[i,j] = c1
#         pcorr[i, j] = c1
# labels = ['FPR D','TPR D','FNR D','TNR D','FOR D','FDR D','PPV D','NPV D','PPREV D','PPR D','BA','A','AUC']
# plt.xticks(np.arange(13),labels)
# plt.yticks(np.arange(13), labels)
# plt.imshow(corr)
# plt.colorbar()
# plt.title('Correlation between different measures, experiment '+args.experiment)
# plt.show
# ()



if args.allsamp:
    ############# Learning Curve on sample size
    if 'savry' in args.experiment:
        if '4000' in args.experiment:
            tsamples=4752/args.kfolds*(args.kfolds-1)
        else:
            tsamples=855/args.kfolds*(args.kfolds-1)
    else:
        tsamples = len(df)/args.kfolds*(args.kfolds-1)
    nsamples = np.round(tsamples * np.round(np.arange(0.1, 1.1, 0.1), 1))
    for approach in approaches:
        aucs = np.array([metrics[k][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'] for k in np.round(np.arange(0.1, 1.1, 0.1),1)])
        if 'vanilla' in approach:
            max_auc = aucs.max()
            aucs = max_auc * np.ones_like(aucs)
            if 'savry' in args.experiment:
                label = "SAVRY overall score"
            else:
                label = "COMPAS score"
            plt.plot(nsamples, aucs, label=label)
        else:
            plt.plot(nsamples, aucs, label="{} on demographics/history".format(approach))#label=approach
    plt.title('Learning curve')
    plt.xlabel('Number of samples')
    plt.ylabel('AUC')
    plt.legend(loc='best')
    plt.show()

#import pdb;pdb.set_trace()
if args.allexp:
    if args.allseed:
        for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
            for approach in approaches:
                aucs1 = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['auc'] for k in
                                  metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                print('{}, {}, mean={}, std={}'.format(exp_name, approach, np.round(np.mean(aucs1), 2),
                                                       np.round(np.std(aucs1), 4)))


#confidence interval with
if not args.allexp:
    if args.allseed:
        ############# Wilcoxon Signed-Rank Test
        ############################# https://machinelearningmastery.com/nonparametric-statistical-significance-tests-in-python/
        for experiment in experiments:
            for a in range(len(approaches)-1):
                aucs1 = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approaches[a]][k]['auc'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approaches[a]].keys()])
                for b in range(a+1,len(approaches)):
                    aucs2 = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approaches[b]][k]['auc'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approaches[b]].keys()])
                    #import pdb;pdb.set_trace()
                    stat, p = wilcoxon(aucs1, aucs2)
                    print('Wilcoxon Signed-Rank Test={}, p={}, ({},{})'.format(np.round(stat,5), np.round(p,5),approaches[a], approaches[b]))

        ############# AUC for different approaches and a given experiment
        for experiment in experiments:
            for approach in approaches:
                if approach == 'vanilla savry':
                    utils.plot_roc_curve(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fpr'],metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['tpr'], hold=True,
                                     label='{}, AUC={}'.format(approach, np.round(metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2)))
                else:
                    #import pdb;pdb.set_trace()
                    rocs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['roc_thresholds'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                    aucs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['auc'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                    utils.plot_roc_curve_ci(rocs[:,:,0],rocs[:,:,1], hold=True,label='{}, AUC={}'.format(approach, np.round(aucs.mean(), 2)))
        plt.show()
    else:
        ############# AUC for different approaches and a given experiment
        for experiment in experiments:
            for approach in approaches:
                utils.plot_roc_curve(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fpr'], metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['tpr'], hold=True,
                        label='{}, AUC={}'.format(approach, np.round(metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2)))
        plt.show()
else:
    if args.allseed:
        ############# AUC for different approaches and multiple experiments
        lines = ["-", ":", "--", "-.","-",":"]
        for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
            for approach in approaches:
                # if j==0 or approach!='vanilla savry':
                #   label = '{}, {}, AUC={}'.format(approach, experiment, np.round(metrics[1][args.experiment][args.kfolds]['onehot'][approach][0]['auc'], 2))
                if approach == 'vanilla savry':
                    label = 'SAVRY overall score, AUC={}'.format(
                        np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2))
                elif approach == 'vanilla compas':
                    label = 'COMPAS overall score, AUC={}'.format(
                        np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2))
                else:
                    label = '{} on {}, AUC={}'.format(approach, exp_name, np.round(
                        metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2))
                if j == 0 or 'vanilla' not in approach:
                    m1 = metrics[1][experiment][args.kfolds][encodings[0]][approach][0]['fpr']
                    m2 = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['tpr']
                    minlen=np.minimum(m1.shape[0],m2.shape[0])
                    utils.plot_roc_curve(m1[:minlen],m2[:minlen],hold=True,label=label, linestyle=lines[j])
                aucs1 = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['auc'] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                # import pdb;
                # pdb.set_trace()
                print('{}, {}, mean={}, std={}'.format(exp_name,approach, np.round(np.mean(aucs1), 2), np.round(np.std(aucs1), 4)))
                if j < len(experiments):
                    for i in range(j + 1, len(experiments)):
                        aucs2 = np.array([metrics[1][experiments[i]][args.kfolds][encodings[0]][approach][k]['auc'] for k in metrics[1][experiments[i]][args.kfolds][encodings[0]][approach].keys()])
                        stat, p = wilcoxon(aucs1, aucs2)
                        print('Wilcoxon Signed-Rank Test={}, p={}, {}, ({},{})'.format(np.round(stat, 5), np.round(p, 5),approach, experiment, experiments[i]))
            if j == 0:
                plt.legend()
            plt.gca().set_prop_cycle(None)
        plt.show()
    else:
        ############# AUC for different approaches and multiple experiments
        lines =  ["-", ":", "--", "-.","-",":"]
        for j,(experiment,exp_name) in enumerate(zip(experiments,experiments_names)):
            for approach in approaches:
                # if j==0 or approach!='vanilla savry':
                #   label = '{}, {}, AUC={}'.format(approach, experiment, np.round(metrics[1][args.experiment][args.kfolds]['onehot'][approach][0]['auc'], 2))
                if approach == 'vanilla savry':
                    label = 'SAVRY overall score, AUC={}'.format(np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2))
                elif approach == 'vanilla compas':
                    label = 'COMPAS overall score, AUC={}'.format(np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2))
                else:
                    label = '{} on {}, AUC={}'.format(approach,exp_name, np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['auc'], 2))
                if j == 0 or 'vanilla' not in approach:
                    utils.plot_roc_curve(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fpr'], metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['tpr'], hold=True,
                        label=label,linestyle=lines[j])
            if j == 0:
                plt.legend()
            plt.gca().set_prop_cycle(None)
        plt.show()

#################GROUP FAIRNESS - bar plots
if args.allexp:
    measures = ['FPRD','FNRD','DD']
    measures_id = [0,2,8]
    approaches_bar = [a.replace('vanilla savry sum', 'SAVRY sum').replace('vanilla savry expert', 'Expert') for a in approaches]
    ############## error rate balance - all experiments
    for pf in metrics[1][experiments[0]][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
        groups = list(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys())
        groups = [g for g in groups if not metrics[samples[0]][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']]
        if 'Other' in groups:
            groups.remove('Other')
        if 'European' in groups:
            groups.remove('European')
        barWidth = 1./len(approaches)-0.05
        f, axarr = plt.subplots(len(measures), sharex=True,figsize=(18, 12))
        for id, measure, idm in zip(measures_id, measures, list(range(len(measures)))):
            for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
                shift_exp = j*len(groups)
                for i, approach in enumerate(approaches):
                    if i==0:
                        ind = np.arange(len(groups))
                    else:
                        ind_prev = ind
                        ind = np.array([xind + barWidth for xind in ind_prev])
                    if args.allseed:
                        means=[np.mean([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][id] for k in
                            metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()]) for g in groups]
                        errors = np.transpose(np.array([np.nan_to_num(list(scipy.stats.t.interval(0.95, len(metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()) - 1, loc=np.mean([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][id] for k in
                            metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()]), scale=scipy.stats.sem([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][id] for k in
                            metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])))) for g in groups]))
                        #import pdb;pdb.set_trace()
                    else:
                        means = [metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'][id] for g in groups]

                    if args.allseed and 'vanilla' not in approach:
                        axarr[idm].bar(shift_exp + ind, means, yerr=np.abs(errors-means),width=barWidth, edgecolor='white', label=approaches_bar[i])
                    else:
                        axarr[idm].bar(shift_exp + ind, means, width=barWidth, edgecolor='white', label=approaches_bar[i])

                if j==0:
                    #plt.legend()
                    plt.legend(bbox_to_anchor=(0,-0.39,1,0), loc="lower left",mode="expand", borderaxespad=0, ncol=len(approaches))
                    #print(j)
                else:
                    #ax1 = plt.gca()
                    axarr[idm].axvline(x=shift_exp-barWidth*0.5, linewidth=2, color='b')
                new_value = (float(j) / len(experiments)) * 0.7 + 0.22
                if idm==0:
                    axarr[idm].text(new_value, 0.9, exp_name, fontsize=18, transform=plt.gcf().transFigure)
                axarr[idm].set_prop_cycle(None)
            plt.xticks([j*len(groups) + g + 0.25 for g in range(len(groups)) for j in range(len(experiments))], [g for g in (groups) for j in range(len(experiments))])
            #ax1 = plt.gca()
            axarr[idm].axhline(y=0.8,linewidth=2, color='r')
            axarr[idm].axhline(y=1.2,linewidth=2, color='r')
            #axarr[id].ylabel(measure)
            axarr[idm].set_ylabel(measure)

        plt.savefig(os.path.join(pardir,'figs',pf+'.pdf'), bbox_inches='tight', pad_inches=0)
        plt.show()




if not args.allexp:
    ############## GROUP FAIRNESS - error rate balance
    for pf in metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
        groups = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
        aucs = np.array([metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'] for g in groups])
        ax1 = plt.gca()
        for i,g in enumerate(groups):
            if not metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
                for approach in approaches:
                    if args.allseed:
                        xs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][0] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                        ys = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][2] for k in metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                        x = np.mean(xs)
                        y = np.mean(ys)
                        intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=x, scale=scipy.stats.sem(xs))))
                        inty = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(ys) - 1, loc=x, scale=scipy.stats.sem(ys))))
                    else:
                        x = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'][0]
                        y = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'][2]

                    if args.allseed:
                        color = next(ax1._get_lines.prop_cycler)['color']
                        if 'vanilla' not in approach:
                            el = patches.Ellipse((x, y), (intx[1]-intx[0]), (inty[1]-inty[0]),fill=True, alpha=0.2,color=color)
                            #el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
                            ax1.add_patch(el)
                            ax1.add_artist(el)
                        plt.scatter(x, y, marker='o', color=color, label='{}, {}, AUCg={}'.format(approach, g, np.round(metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'], 2)),s=5)
                    else:
                        size =  (aucs[i] - aucs.min()) / (aucs.max() - aucs.min())
                        plt.scatter(x,y, marker='o',  label='{}, {}, AUCg={}'.format(approach,g, np.round(metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'], 2)),
                                    s=200*(size+0.1))
                    plt.text(x * (1 + 0.01), y * (1 + 0.01), g, fontsize=8)
                plt.gca().set_prop_cycle(None)

        for i, g in enumerate(groups):
            for approach in approaches:
                aucg = metrics[1][args.experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc']
                print('{}, {}, AUCg={}'.format(approach,g, np.round(aucg, 2)))

        plt.title('Error rate balance (Chouldechova 2017)')
        plt.xlabel("FPR Disparity")
        plt.ylabel("FNR Disparity")
        plt.legend()
        ax1.add_patch(matplotlib.patches.Rectangle((0.8, 0.8), 0.4, 0.4,alpha=0.1, facecolor='green'))
        plt.show()

else:
    ############## GROUP FAIRNESS - error rate balance - all experiments
    markers=['o','s','^','P','D','X','H']
    for pf in metrics[1][experiments[0]][args.kfolds][encodings[0]][approach][seeds[0]]['fair']:
        ax1 = plt.gca()
        for j, (experiment, exp_name) in enumerate(zip(experiments, experiments_names)):
            groups = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf].keys()
            aucs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'] for g in groups])
            for i,g in enumerate(groups):
                if not metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['reference']:
                    for approach in approaches:
                        if j == 0 and 'vanilla' in approach:
                            if approach == 'vanilla savry':
                                label = 'SAVRY overall score,{}, AUC={}'.format(g, np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'], 2))
                            elif approach == 'vanilla compas':
                                label = 'COMPAS overall score,{}, AUC={}'.format(g, np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'], 2))
                        else:
                            label = '{} on {}, {}, AUCg={}'.format(approach, exp_name, g, np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc'], 2))
                        if args.allseed:
                            #import pdb;pdb.set_trace()
                            xs = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][0] for k in
                                           metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                            ys = np.array([metrics[1][experiment][args.kfolds][encodings[0]][approach][k]['fair'][pf][g]['disparity'][1] for k in
                                           metrics[1][experiment][args.kfolds][encodings[0]][approach].keys()])
                            x = np.mean(xs)
                            y = np.mean(ys)
                            intx = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(xs) - 1, loc=x, scale=scipy.stats.sem(xs))))
                            inty = np.nan_to_num(list(scipy.stats.t.interval(0.95, len(ys) - 1, loc=x, scale=scipy.stats.sem(ys))))
                        else:
                            x = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'][0]
                            y = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['disparity'][2]

                        if args.allseed:
                            color = next(ax1._get_lines.prop_cycler)['color']
                            if 'vanilla' not in approach:
                                el = patches.Ellipse((x, y), (intx[1] - intx[0]), (inty[1] - inty[0]),color=color, fill=True,alpha=0.2)
                                # el = patches.Ellipse((x, y), 0.06, 0.06, fill=True)
                                ax1.add_patch(el)
                                ax1.add_artist(el)
                            if j == 0 or 'vanilla' not in approach:
                                plt.scatter(x, y,color=color, marker=markers[j], label='{}, {}, {} '.format(experiment,approach, g), s=5) #  np.round(metrics[1][experiment][args.kfolds][encodings[0]][approach][seed]['fair'][pf][g]['auc'], 2)
                        else:
                            size =  (aucs[i] - aucs.min()) / (aucs.max() - aucs.min())
                            if j == 0 or 'vanilla' not in approach:
                                plt.scatter(x,y, marker=markers[j], label=label,s=200*(size+0.1))
                        plt.text(x * (1 + 0.01), y * (1 + 0.01), g, fontsize=8)

                    plt.gca().set_prop_cycle(None)
            for i, g in enumerate(groups):
                for approach in approaches:
                    aucg = metrics[1][experiment][args.kfolds][encodings[0]][approach][seeds[0]]['fair'][pf][g]['auc']
                    print('{}, {}, {}'.format(experiment,approach,g))
        plt.title('Error rate balance (Chouldechova 2017)')
        plt.xlabel("FPR Disparity")
        plt.ylabel("FNR Disparity")
        plt.legend()
        ax1 = plt.gca()
        ax1.add_patch(matplotlib.patches.Rectangle((0.8, 0.8), 0.4, 0.4,alpha=0.1, facecolor='green'))
        plt.show()


# ############## Learning Curve on K folds
# for approach in approaches:
#     aucs = np.array([metrics[1][args.experiment][k]['onehot'][approach][0]['auc'] for k in range(2,14)])
#     if approach=='vanilla savry':
#         max_auc = aucs.max()
#         aucs = max_auc * np.ones_like(aucs)
#     plt.title('Learning curve for K folds, experiment '+args.experiment)
#     plt.plot( list(range(2,14)), aucs, label=approach)
#     plt.xlabel('Number of folds K')
#     plt.ylabel('AUC')
#     plt.legend(loc='best')
# plt.show()




        # # acc_all=np.array(acc_all)
        # # auc_all=np.array(auc_all)
        # # p_all = np.array(p_all)
        # # r_all = np.array(r_all)
        # # avp_all = np.array(avp_all)
        # for i in range(len(approaches)):
        #     utils.plot_roc_curve(fpr_all[i], tpr_all[i], hold=True,label='{}, ACC={}, AUC={}'.format(approaches[i], np.round(acc_all[i], 2),np.round(auc_all[i], 2)))
        #     #utils.plot_pr_curve(p_all[i], r_all[i],label='{}, ACC={}, AVP={}'.format(approaches[i], np.round(acc_all[i], 2),np.round(avp_all[i], 2)))
        #     #import pdb;pdb.set_trace()
        # plt.show()
        #
        #
        #
        # thresholds = np.flip(np.linspace(1, 0, 101))
        # #plt.title('Balanced accuracy')
        # for i in range(len(approaches)):
        #     plt.plot(thresholds, ROC1_all[i][:,10], label=approaches[i])
        # # plt.xlim([0.0, 1.0])
        # # plt.ylim([0.0, 1.05])
        # plt.xlabel('Threshold')
        # plt.ylabel('Balanced accuracy')
        # plt.legend(loc='best')
        # plt.show()
        #
        #
        #
        #
        # for k in range(len(protected)):
        #     groups = list(df[protected[k]].unique())
        #     counts = df[protected[k]].value_counts()
        #     reference_group = counts.idxmax()
        #     reference_group_idx = groups.index(reference_group)
        #
        #     for i in range(len(approaches)):
        #         fig, ax1 = plt.subplots()
        #         ax2 = ax1.twinx()
        #         ymin=1e8
        #         ymax=0
        #         for j in range(len(groups)):
        #             if j != reference_group_idx:
        #                 ymin = np.minimum(ymin,np.minimum(ROC_disparity_all[i][k][j][:,0], ROC_all[i][k][j][:,10]).min() )
        #                 ymax = np.maximum(ymax,np.maximum(ROC_disparity_all[i][k][j][:,0], ROC_all[i][k][j][:,10]).max() )
        #                 utils.plot_2d(ax1, ax2, ROC_disparity_all[i][k][j][:, 0], ROC_all[i][k][j][:, 10], 'FPR', 'BA',label='{}'.format(groups[j]))
        #                 #utils.plot_2d(ax1,ax2,ROC_disparity_all[i][k][j][:,0], ROC_all[i][k][j][:,10], 'FPR', 'BA',label='{}, ACC={}, AUC={}'.format(approaches[i], np.round(acc_all[i], 2), np.round(auc_all[i], 2)))
        #                 #utils.plot_roc_curve(fpr_all[i], tpr_all[i], hold=True, label='{}, ACC={}, AUC={}'.format(approaches[i], np.round(acc_all[i], 2), np.round(auc_all[i], 2)))
        #
        #         ax1.legend()
        #         ax1.set_title('Eval for {}, ACC={}, AUC={}'.format(approaches[i], np.round(acc_all[i], 2),np.round(auc_all[i], 2)))
        #         #import pdb;pdb.set_trace()
        #         #ax1.set_title('Fairness for {}'.format(groups[j]))
        #         #ax1.set_ylim([ymin-0.1, ymax+0.1])
        #         #ax2.set_ylim([ymin-0.1, ymax+0.1])
        #         plt.show()
        #
        # out_file = '../res/' + args.experiment + '_' + str(encodings[0]) + '.txt'
        # with open(out_file, 'w') as f:
        #     f.write(buffer)
