#!/bin/sh


approach=( linearsvm naivebayes knn tree forest  )
experiment=( exp1_demogsavry exp1_demog exp1_savry)

## get item count using ${approach[@]} ##

for (( s=$1; s <= $1; s++ ))
  do
for (( m=0; m <= 2; m++ ))
  do
for (( k=0; k <= 1; k++ ))
  do
for e in "${experiment[@]}"
do
 echo "Experiment $e"
 for a in "${approach[@]}"
 do
  echo "Approach $a"
  for (( i=0; i <= 9; i++ ))
  do
   python main.py --kfolds 10 --fold "$i" --experiment "$e" --approach "$a" --seed "$s" --mitigate "$m" --protected_id "$k"
  done
 done
done
done
done
done
