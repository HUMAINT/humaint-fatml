#!/bin/sh

approach=( linearsvm naivebayes knn tree forest )
experiment=( exp1_savry exp1_demog exp1_demogsavry )

## get item count using ${approach[@]} ##
for (( s=0; s <= 0; s++ ))
  do
for e in "${experiment[@]}"
do
 echo "Experiment $e"
 for a in "${approach[@]}"
 do
  echo "Approach $a"
  for (( i=0; i <= 9; i++ ))
  do
   python main.py --kfolds 10 --interpretability 0 --fold "$i" --experiment "$e" --approach "$a" --seed "$s"
  done
 done
done
done

approach=( logit mlp )
experiment=( exp1_savry exp1_demog exp1_demogsavry )

## get item count using ${approach[@]} ##
for (( s=0; s <= 0; s++ ))
  do
for e in "${experiment[@]}"
do
 echo "Experiment $e"
 for a in "${approach[@]}"
 do
  echo "Approach $a"
  for (( i=0; i <= 9; i++ ))
  do
   python main.py --kfolds 10 --fold "$i" --experiment "$e" --approach "$a" --seed "$s"
  done
 done
done
done
