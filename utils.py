import os,sys
import numpy as np
from copy import deepcopy
import torch
import scipy
import pandas as pd
from scipy.stats import pearsonr
import configparser
import ast

def equalize_other(X,y,train_sensitive_idx,k=5,Ax=0.01,Ay=1.0,Az=50.0, print_interval=250, verbose=1):
    import lfr_helpers
    import scipy.optimize as optim

    training_sensitive = X[train_sensitive_idx]
    training_nonsensitive = X[~train_sensitive_idx]
    ytrain_sensitive = y[train_sensitive_idx]
    ytrain_nonsensitive = y[~train_sensitive_idx]

    num_train_samples, features_dim = X.shape
    model_inits = np.random.uniform(size=features_dim * 2 + k + features_dim * k)

    bnd = []
    for i, _ in enumerate(model_inits):
        if i < features_dim * 2 or i >= features_dim * 2 + k:
            bnd.append((None, None))
        else:
            bnd.append((0, 1))

    learned_model = optim.fmin_l_bfgs_b(lfr_helpers.LFR_optim_obj, x0=model_inits, epsilon=1e-5,
                              args=(training_sensitive, training_nonsensitive,
                                    ytrain_sensitive, ytrain_nonsensitive, k, Ax,
                                    Ay, Az, 0, print_interval),
                              bounds=bnd, approx_grad=True, maxfun=5000,
                              maxiter=5000, disp=verbose)[0]

    return learned_model

def transform_features(X,y,test_sensitive_idx,learned_model, k=5,Ax=0.01,Ay=1.0,Az=50.0, print_interval=250, verbose=1, threshold=0.5):
    import lfr_helpers
    testing_sensitive = X[test_sensitive_idx]
    testing_nonsensitive = X[~test_sensitive_idx]
    ytest_sensitive = y[test_sensitive_idx]
    ytest_nonsensitive = y[~test_sensitive_idx]

    # extract training model parameters
    Ns, P = testing_sensitive.shape
    N, _ = testing_nonsensitive.shape
    alphaoptim0 = learned_model[:P]
    alphaoptim1 = learned_model[P: 2 * P]
    woptim = learned_model[2 * P: (2 * P) + k]
    voptim = np.matrix(learned_model[(2 * P) + k:]).reshape((k, P))

    # compute distances on the test dataset using train model params
    dist_sensitive = lfr_helpers.distances(testing_sensitive, voptim, alphaoptim1, Ns, P, k)
    dist_nonsensitive = lfr_helpers.distances(testing_nonsensitive, voptim, alphaoptim0, N, P, k)

    # compute cluster probabilities for test instances
    M_nk_sensitive = lfr_helpers.M_nk(dist_sensitive, Ns, k)
    M_nk_nonsensitive = lfr_helpers.M_nk(dist_nonsensitive, N, k)

    # learned mappings for test instances
    res_sensitive = lfr_helpers.x_n_hat(testing_sensitive, M_nk_sensitive, voptim, Ns, P, k)
    x_n_hat_sensitive = res_sensitive[0]
    res_nonsensitive = lfr_helpers.x_n_hat(testing_nonsensitive, M_nk_nonsensitive, voptim, N, P, k)
    x_n_hat_nonsensitive = res_nonsensitive[0]

    # compute predictions for test instances
    res_sensitive = lfr_helpers.yhat(M_nk_sensitive, ytest_sensitive, woptim, Ns, k)
    y_hat_sensitive = res_sensitive[0]
    res_nonsensitive = lfr_helpers.yhat(M_nk_nonsensitive, ytest_nonsensitive, woptim, N, k)
    y_hat_nonsensitive = res_nonsensitive[0]

    transformed_features = np.zeros(shape=X.shape)
    transformed_labels = np.zeros(shape=y.shape)
    transformed_features[test_sensitive_idx] = x_n_hat_sensitive
    transformed_features[~test_sensitive_idx] = x_n_hat_nonsensitive
    try:
        transformed_labels[test_sensitive_idx] = np.reshape(y_hat_sensitive,[-1, 1])
        transformed_labels[~test_sensitive_idx] = np.reshape(y_hat_nonsensitive,[-1, 1])
        transformed_labels = (np.array(transformed_labels) > threshold).astype(np.float64)
    except Exception as e:
        pass
        #print('error in utils line 83')
        #import pdb;pdb.set_trace()

    return transformed_features,transformed_labels

def transform_interpret(X,learned_model,protected_mapped,reference_value):
    test_sensitive_idx = X[:,protected_mapped]!=int(reference_value)
    transformed_features,transformed_labels = transform_features(X,np.ones(X.shape[0]),test_sensitive_idx,learned_model)
    return transformed_features

############################################################################
def load_config(experiment):
    # Load the configuration file
    with open(os.path.join('experiments',experiment+".ini"), encoding = "utf-8") as f:
        config = configparser.RawConfigParser(allow_no_value=True)
        config.read_file(f)

    for ids,section in enumerate(['dataset','experiment']):
        assert section in config.sections(),"Config file does not have the section: "+section
        if ids==0:
            options = ['filename','url','filename_test']
        else:
            options = ['label_column','protected_features','drop_train','drop_columns','group_column','code_numerical','column_id']
        for option in options:
            #import pdb;pdb.set_trace()
            if option not in ['filename_test','drop_train','drop_columns','group_column','code_numerical','column_id']:
                assert option in config.options(section),"Config file does not have the option: "+option+" at section "+section
                # assert len(config.get(section, option))>0, "No content for the option: "+option+" at section "+section
            elif option not in config.options(section):
                config.set(section, option,None)
            elif len(config.get(section, option))<1:
                config.set(section, option,None)
            if config.get(section, option) is not None and ('[' in config.get(section, option) or ',' in config.get(section, option)):
                config.set(section, option,ast.literal_eval(config.get(section, option)))

    return config

def get_config(config,section,element):
    try:
        param = config.get(section,element)
    except Exception as e:
        param = None
    return param

######################################################################

def print_model_report(model):
    print('-'*100)
    print(model)
    print('Dimensions =',end=' ')
    count=0
    for p in model.parameters():
        print(p.size(),end=' ')
        count+=np.prod(p.size())
    print()
    print('Num parameters = %s'%(human_format(count)))
    print('-'*100)
    return count

def human_format(num):
    magnitude=0
    while abs(num)>=1000:
        magnitude+=1
        num/=1000.0
    return '%.1f%s'%(num,['','K','M','G','T','P'][magnitude])

def print_optimizer_config(optim):
    if optim is None:
        print(optim)
    else:
        print(optim,'=',end=' ')
        opt=optim.param_groups[0]
        for n in opt.keys():
            if not n.startswith('param'):
                print(n+':',opt[n],end=', ')
        print()
    return
######################################################################
def plot_pr_curve(precision, recall, label=None,hold=False):
    """
    The ROC curve, modified from
    Hands-On Machine learning with Scikit-Learn and TensorFlow; p.91
    """
    import matplotlib
    matplotlib.use('TKAgg')
    import matplotlib.pyplot as plt
    #import pdb;pdb.set_trace()
    #plt.figure(figsize=(8,8))

    f_scores = np.linspace(0.2, 0.8, num=4)
    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    plt.title('2-class Precision-Recall curve')
    plt.plot(recall, precision, label=label)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.legend(loc='best')
    # if not hold:
    #     plt.show()

def plot_roc_curve(fpr, tpr, label=None,hold=False,linestyle=None):
    """
    The ROC curve, modified from
    Hands-On Machine learning with Scikit-Learn and TensorFlow; p.91
    """
    import matplotlib
    matplotlib.use('TKAgg')
    import matplotlib.pyplot as plt
    #import pdb;pdb.set_trace()
    #plt.figure(figsize=(8,8))
    plt.title('ROC Curve')
    if linestyle is not None:
        plt.plot(fpr, tpr, linewidth=2, label=label,linestyle=linestyle)
    else:
        plt.plot(fpr, tpr, linewidth=2, label=label)
    #plt.plot([0, 1], [0, 1], 'k--')
    #plt.axis([-0.005, 1, 0, 1.005])
    #plt.xticks(np.arange(0,1, 0.05), rotation=90)
    plt.xlabel("False Positive Rate (Precision)")
    plt.ylabel("True Positive Rate (Recall)")
    plt.legend(loc='best')
    if not hold:
        plt.show()


def plot_roc_curve_ci(fpr_all, tpr_all, label=None,hold=False,linestyle=None):
    """
    The ROC curve, modified from
    Hands-On Machine learning with Scikit-Learn and TensorFlow; p.91
    """
    import matplotlib
    matplotlib.use('TKAgg')
    import matplotlib.pyplot as plt

    #plt.figure(figsize=(8,8))
    plt.title('ROC Curve')
    mean_fpr = np.mean(fpr_all,axis=0)
    #ci_fpr = np.array([list(scipy.stats.t.interval(0.95, len(fpr_all[:,k]) - 1, loc=np.mean(fpr_all[:,k]), scale=scipy.stats.sem(fpr_all[:,k]))) for k in range(fpr_all.shape[1])])
    mean_tpr = np.mean(tpr_all, axis=0)
    ci_tpr = np.array([list(scipy.stats.t.interval(0.95, len(tpr_all[:,k]) - 1, loc=np.mean(tpr_all[:,k]), scale=scipy.stats.sem(tpr_all[:,k]))) for k in range(tpr_all.shape[1])])

    ax = plt.gca()
    color = next(ax._get_lines.prop_cycler)['color']
    # import pdb;
    # pdb.set_trace()
    if linestyle is not None:
        plt.plot(mean_fpr, mean_tpr, linewidth=1, label=label,linestyle=linestyle,color=color)
    else:
        plt.plot(mean_fpr, mean_tpr, linewidth=1, label=label,color=color)
    plt.fill_between(mean_fpr, ci_tpr[:,0], ci_tpr[:,1],alpha=.2,color=color)
    #plt.plot([0, 1], [0, 1], 'k--')
    #plt.axis([-0.005, 1, 0, 1.005])
    #plt.xticks(np.arange(0,1, 0.05), rotation=90)
    plt.xlabel("False Positive Rate (Precision)")
    plt.ylabel("True Positive Rate (Recall)")
    plt.legend(loc='best')
    if not hold:
        plt.show()

def plot_2d_fairness(x,y, text,size,label=None,hold=False):
    import matplotlib
    matplotlib.use('TKAgg')
    import matplotlib.pyplot as plt
    # plt.scatter(data[:, 0], data[:, 1], marker='o', c=data[:, 2], s=data[:, 3] * 1500,cmap=plt.get_cmap('Spectral'))
    #import pdb;pdb.set_trace()
    plt.plot(x,y, marker='o', label=label,s=size)
    for i in range(len(x)):
        plt.text(x[i] * (1 + 0.01), y[i] * (1 + 0.01), text[i], fontsize=9)
    plt.xlabel("PPREV Disparity")
    plt.ylabel("FPR Disparity")
    plt.legend()
    if not hold:
        plt.show()

def plot_2d(ax1,ax2,x,y,text1,text2,label=None):
    import matplotlib
    matplotlib.use('TKAgg')
    import matplotlib.pyplot as plt

    thresholds = np.linspace(1, 0, 101)

    #import pdb;pdb.set_trace()
    ax1.plot(thresholds,x,label=label)
    ax2.plot(thresholds,y,dashes=[6, 2])
    ax1.set_xlabel('Threshold')
    ax1.set_ylabel(text1)
    ax2.set_ylabel(text2)

def compute_roc(T, Y, t, ntotal):
    # Classifier / label agree and disagreements for current threshold.
    TP_t = np.logical_and(T > t, Y == 1).sum()
    TN_t = np.logical_and(T <= t, Y == 0).sum()
    FP_t = np.logical_and(T > t, Y == 0).sum()
    FN_t = np.logical_and(T <= t, Y == 1).sum()

    ROC = np.zeros(12)
    #import pdb;pdb.set_trace()

    # Compute false positive rate for current threshold.
    FPR_t = np.divide(float(FP_t), float(FP_t + TN_t), out=np.zeros_like(FP_t).astype(np.float32),where=(FP_t + TN_t) != 0)
    ROC[0] = FPR_t

    # Compute true positive rate for current threshold.
    TPR_t = np.divide(float(TP_t), float(TP_t + FN_t), out=np.zeros_like(TP_t).astype(np.float32), where=(TP_t + FN_t) != 0)
    ROC[1] = TPR_t

    # Compute false negative rate for current threshold.
    FNR_t = np.divide(float(FN_t), float(TP_t + FN_t), out=np.zeros_like(FN_t).astype(np.float32),where=(TP_t + FN_t) != 0)
    ROC[2] = FNR_t

    # Compute true negative rate for current threshold.
    TNR_t = np.divide(float(TN_t), float(FP_t + TN_t), out=np.zeros_like(TN_t).astype(np.float32),where=(FP_t + TN_t) != 0)
    ROC[3] = TNR_t

    # Compute false omission rate for current threshold.
    FOR_t = np.divide(float(FN_t), float(FN_t + TN_t), out=np.zeros_like(FN_t).astype(np.float32),where=(FN_t + TN_t) != 0)
    ROC[4] = FOR_t

    # Compute false discovery rate for current threshold.
    FDR_t = np.divide(float(FP_t), float(TP_t + FP_t), out=np.zeros_like(FP_t).astype(np.float32),where=(TP_t + FP_t) != 0)
    ROC[5] = FDR_t

    # Compute precision or positive predictive value for current threshold.
    PPV_t = np.divide(float(TP_t), float(TP_t + FP_t), out=np.zeros_like(TP_t).astype(np.float32),where=(TP_t + FP_t) != 0)
    ROC[6] = PPV_t

    # Compute negative predictive value for current threshold.
    NPV_t = np.divide(float(TN_t), float(TN_t + FN_t), out=np.zeros_like(TN_t).astype(np.float32),where=(TN_t + FN_t) != 0)
    ROC[7] = NPV_t

    # Compute predicted prevalence for current threshold.
    PPREV_t = float(TP_t + FP_t) / float(len(T))
    ROC[8] = PPREV_t

    # Compute predicted positive rate for current threshold.
    PPR_t = float(TP_t + FP_t) / float(ntotal)
    ROC[9] = PPR_t

    # Compute balanced accuracy for current threshold.
    BA_t = 0.5 * (TPR_t + TNR_t)
    ROC[10] = BA_t

    # Compute balanced accuracy for current threshold.
    A_t = (TP_t + TN_t) / (TP_t + TN_t + FP_t + FN_t)
    ROC[11] = A_t

    return ROC

def compute_roc_thresholds(T, Y, ntotal):
    thresholds = np.linspace(1,0,101)

    ROC = [compute_roc(T, Y, t, ntotal) for t in thresholds]
    ROC = np.array(ROC)

    return ROC

def corrcoef_loop(matrix):
    rows, cols = matrix.shape[0], matrix.shape[1]
    r = np.ones(shape=(rows, rows))
    p = np.ones(shape=(rows, rows))
    for i in range(rows):
        for j in range(i+1, rows):
            r_, p_ = pearsonr(matrix[i], matrix[j])
            #r_ = np.corrcoef(matrix[i], matrix[j])
            p_ = None
            r[i, j] = r[j, i] = r_
            #r[i, j] = r[j, i] = r_[1,0]
            p[i, j] = p[j, i] = p_
    return r, p


######################################################################

def get_model(model):
    return deepcopy(model.state_dict())

def set_model_(model,state_dict):
    model.load_state_dict(deepcopy(state_dict))
    return

def freeze_model(model):
    for param in model.parameters():
        param.requires_grad = False
    return

######################################################################
def get_column_type(df):
    char_cols = df.dtypes.pipe(lambda x: x[x == 'object']).index
    numerical = df._get_numeric_data().columns
    binaries = [c for c in df.columns.values if c not in numerical and len(df[c].unique()) <= 2]
    categorical = [c for c in df.columns.values if c not in numerical and len(df[c].unique()) > 2]
    return numerical,binaries,categorical

def get_column_type_ids(df):
    char_cols = df.dtypes.pipe(lambda x: x[x == 'object']).index
    numerics_temp = df._get_numeric_data().columns
    numerical =[df.columns.get_loc(num) for num in numerics_temp]
    binaries = [idc for idc,c in enumerate(df.columns.values) if c not in numerics_temp and len(df[c].unique()) <= 2]
    categorical = [idc for idc,c in enumerate(df.columns.values) if c not in numerics_temp and len(df[c].unique()) > 2]
    return numerical,binaries,categorical

def preprocessing(df,encoding,numerics,binaries,categorical):
    import sklearn.preprocessing
    from sklearn_pandas import DataFrameMapper
    import category_encoders as ce
    cat_encoding = {
        "backward": ce.BackwardDifferenceEncoder,
        "binary": ce.BinaryEncoder,
        "hashing": ce.HashingEncoder,
        "helmert": ce.HelmertEncoder,
        "onehot": ce.OneHotEncoder,
        "ordinal": ce.OrdinalEncoder,
        "sum": ce.SumEncoder,
        "poly": ce.PolynomialEncoder,
        "basen": ce.BaseNEncoder,
        "target": ce.TargetEncoder,
        "looe": ce.LeaveOneOutEncoder,
    }

    ''' Data pre-processing
    http://pbpython.com/categorical-encoding.html
    http://www.willmcginnis.com/2015/11/29/beyond-one-hot-an-exploration-of-categorical-variables/
    https://stackoverflow.com/questions/24745879/how-to-apply-preprocessing-methods-on-several-columns-at-one-time-in-sklearn
    '''
    return DataFrameMapper(
        [(binary, sklearn.preprocessing.LabelBinarizer()) for binary in binaries] +
        [(encoder, [cat_encoding[encoding](),sklearn.preprocessing.StandardScaler()]) for encoder in categorical] +
        [([scalar], sklearn.preprocessing.StandardScaler()) for scalar in numerics]
    )
    #use RobustScaler if data has many outliers

def preprocess_array(data,encoding,numericsids,binariesids,categoricalids):
    import sklearn.preprocessing
    import category_encoders as ce
    cat_encoding = {
        "backward": ce.BackwardDifferenceEncoder,
        "binary": ce.BinaryEncoder,
        "hashing": ce.HashingEncoder,
        "helmert": ce.HelmertEncoder,
        "onehot": ce.OneHotEncoder,
        "ordinal": ce.OrdinalEncoder,
        "sum": ce.SumEncoder,
        "poly": ce.PolynomialEncoder,
        "basen": ce.BaseNEncoder,
        "target": ce.TargetEncoder,
        "looe": ce.LeaveOneOutEncoder,
    }

    le1 = cat_encoding[encoding]()
    for feature in categoricalids+binariesids:
        le1 = cat_encoding[encoding]()
        le1.fit(data[:, feature])
        data[:, feature] = np.ravel(le1.transform(data[:, feature]))

    le2 = sklearn.preprocessing.StandardScaler()
    for feature in numericsids+categoricalids:
        le2 = sklearn.preprocessing.StandardScaler()
        le2.fit(data[:, feature:feature+1])
        data[:, feature] = np.ravel(le2.transform(data[:, feature:feature+1]))

    # for feature in binariesids:
    #     le3 = sklearn.preprocessing.LabelBinarizer()
    #     le3.fit(data[:, feature])
    #     data[:, feature] = np.ravel(le3.transform(data[:, feature]))

    return data,le1,le2

def preprocess_element(data,encoding,numericsids,binariesids,categoricalids,le1,le2):
    import sklearn.preprocessing
    import category_encoders as ce
    cat_encoding = {
        "backward": ce.BackwardDifferenceEncoder,
        "binary": ce.BinaryEncoder,
        "hashing": ce.HashingEncoder,
        "helmert": ce.HelmertEncoder,
        "onehot": ce.OneHotEncoder,
        "ordinal": ce.OrdinalEncoder,
        "sum": ce.SumEncoder,
        "poly": ce.PolynomialEncoder,
        "basen": ce.BaseNEncoder,
        "target": ce.TargetEncoder,
        "looe": ce.LeaveOneOutEncoder,
    }
    if len(data.shape)<2:
        data = data[np.newaxis,:]
    for feature in categoricalids+binariesids:
        #le1 = cat_encoding[encoding]()
        #le1.fit(data[:, feature])
        data[:,feature:feature+1] = le1.transform(data[:,feature:feature+1])
    for feature in numericsids+categoricalids:
        #le2 = sklearn.preprocessing.StandardScaler()
        #le2.fit(data[:, feature])
        data[:,feature:feature+1] = le2.transform(data[:,feature:feature+1])
    return data

def predict_sklearn(best_clf,X):
    temp1 = best_clf.predict_proba(X)
    temp2 = np.abs(np.array([1, 0]) - temp1)
    scores = temp2[:, 0]
    # import pdb;
    # pdb.set_trace()
    return scores

######################################################################
def compute_fairness_metrics(df,protected_features=[]):
    from aequitas.group import Group
    from aequitas.bias import Bias
    from aequitas.fairness import Fairness
    from aequitas.preprocessing import preprocess_input_df

    df,columns=preprocess_input_df(df)

    frequent = [df[f].mode() for i, f in enumerate(protected_features)]
    dict_protected = {}
    for i in range(len(protected_features)):
        dict_protected[protected_features[i]] = frequent[i][0]
    used_features = protected_features + ['score','label_value']
    # import pdb;
    # pdb.set_trace()
    dfs = df[used_features]


    g = Group()
    xtab, _ = g.get_crosstabs(dfs)
    #print(xtab[['attribute_name', 'attribute_value', 'fpr', 'fnr', 'tpr', 'tnr', 'for', 'fdr', 'group_size']].round(2))

    b = Bias()
    bdf = b.get_disparity_predefined_groups(xtab, dict_protected)
    print(bdf[['attribute_name', 'attribute_value', 'ppr_disparity', 'pprev_disparity', 'fdr_disparity', 'for_disparity',
         'fpr_disparity', 'fnr_disparity']])

    f = Fairness()
    fdf = f.get_group_value_fairness(bdf)

    print(fdf[['attribute_name', 'attribute_value','Statistical Parity',
       'Impact Parity', 'FDR Parity', 'FPR Parity', 'FOR Parity', 'FNR Parity',
       'TypeI Parity', 'TypeII Parity', 'Unsupervised Fairness',
       'Supervised Fairness']])

    gaf = f.get_group_attribute_fairness(fdf)
    return xtab,bdf,fdf,gaf


######################################################################
def weighted_binary_cross_entropy(output, target, weight=None):
    output = torch.clamp(output, min=1e-8, max=1 - 1e-8)
    if weight is not None:
        assert len(weight) == 2

        loss = weight[1] * (target * torch.log(output)) + \
               weight[0] * ((1 - target) * torch.log(1 - output))
    else:
        loss = target * torch.log(output) + (1 - target) * torch.log(1 - output)

    return torch.neg(torch.mean(loss))


def weighted_binary_cross_entropy(sigmoid_x, targets, pos_weight, weight=None, size_average=True, reduce=True):
    """
    Args:
        sigmoid_x: predicted probability of size [N,C], N sample and C Class. Eg. Must be in range of [0,1], i.e. Output from Sigmoid.
        targets: true value, one-hot-like vector of size [N,C]
        pos_weight: Weight for postive sample
    """
    if not (targets.size() == sigmoid_x.size()):
        raise ValueError("Target size ({}) must be the same as input size ({})".format(targets.size(), sigmoid_x.size()))

    loss = -pos_weight* targets * sigmoid_x.log() - (1-targets)*(1-sigmoid_x).log()

    if weight is not None:
        loss = loss * weight

    if not reduce:
        return loss
    elif size_average:
        return loss.mean()
    else:
        return loss.sum()


class WeightedBCELoss(torch.nn.Module):
    def __init__(self, pos_weight=1., weight=None, PosWeightIsDynamic= False, WeightIsDynamic= False, size_average=True, reduce=True):
        """
        Args:
            pos_weight = Weight for postive samples. Size [1,C]
            weight = Weight for Each class. Size [1,C]
            PosWeightIsDynamic: If True, the pos_weight is computed on each batch. If pos_weight is None, then it remains None.
            WeightIsDynamic: If True, the weight is computed on each batch. If weight is None, then it remains None.
        """
        super().__init__()

        self.register_buffer('weight', weight)
        self.register_buffer('pos_weight', pos_weight)
        self.size_average = size_average
        self.reduce = reduce
        self.PosWeightIsDynamic = PosWeightIsDynamic

    def forward(self, input, target):
        # pos_weight = Variable(self.pos_weight) if not isinstance(self.pos_weight, Variable) else self.pos_weight
        if self.PosWeightIsDynamic:
            positive_counts = target.sum(dim=0)
            nBatch = len(target)
            self.pos_weight = (nBatch - positive_counts)/(positive_counts +1e-5)

        if self.weight is not None:
            # weight = Variable(self.weight) if not isinstance(self.weight, Variable) else self.weight
            return weighted_binary_cross_entropy(input, target,
                                                 self.pos_weight,
                                                 weight=self.weight,
                                                 size_average=self.size_average,
                                                 reduce=self.reduce)
        else:
            return weighted_binary_cross_entropy(input, target,
                                                 self.pos_weight,
                                                 weight=None,
                                                 size_average=self.size_average,
                                                 reduce=self.reduce)

def pca_code(data):
    #raw_implementation
    var_per=.98
    data-=np.mean(data, axis=0)
    # data/=np.std(data, axis=0)
    cov_mat=np.cov(data, rowvar=False)
    evals, evecs = np.linalg.eigh(cov_mat)
    idx = np.argsort(evals)[::-1]
    evecs = evecs[:,idx]
    evals = evals[idx]
    variance_retained=np.cumsum(evals)/np.sum(evals)
    index=np.argmax(variance_retained>=var_per)
    evecs = evecs[:,:index+1]
    reduced_data=np.dot(evecs.T, data.T).T

    print("evals", evals)
    print("_"*30)
    print(evecs.T)
    print("_"*30)
    #using scipy package
    from sklearn.decomposition import PCA as PCA
    clf=PCA(var_per)
    X_train=data
    X_train=clf.fit_transform(X_train)
    print(clf.explained_variance_)
    print("_"*30)
    print(clf.components_)
    print("__"*30)
    import pdb;
    pdb.set_trace()

def aggregate_cat(explain_df,categ_df):
    agreggate = {}
    for column in categ_df.columns:
        for value in categ_df[column].unique():
            agreggate[value] = explain_df[column][categ_df[column]==value].mean()
    #agreggate = {value: explain_df[column][categ_df[column] == value].mean() for column in categ_df.columns for value in categ_df[column].unique()}
    # import pdb;
    # pdb.set_trace()
    return pd.Series(agreggate, index=agreggate.keys())

def get_categories(series):
    if series.dtype == np.number:
        return range(int(series.min()), int(series.max()) + 1)
    else:
        return series.unique()
